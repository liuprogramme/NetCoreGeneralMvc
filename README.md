# NetCoreGeneralMvc

#### 介绍
NetCoreGeneral是基于.Net6快速开发基础框架，其核心设计目标是开发迅速、代码量少、学习简单、功能强大、轻量级、易扩展，让Web开发更迅速、简单。能解决60%重复工作。为您节约更多时间，去陪恋人、家人和朋友。轻松开发，专注您的业务，从NetCoreGeneral开始！

#### CentOS 7 

 rpm -Uvh https://packages.microsoft.com/config/centos/7/packages-microsoft-prod.rpm
 
 yum install dotnet-sdk-6.0

#### 安装图形GDI

yum install libgdiplus-devel -y

ln -s /usr/lib64/libgdiplus.so /usr/lib/gdiplus.dll

ln -s /usr/lib64/libgdiplus.so /usr/lib64/gdiplus.dll


#### 导入数据库后运行

nohup dotnet General.Web.dll &


#### SkyWalking安装
	https://archive.apache.org/dist/skywalking 下载
	windows下,打开apache-skywalking-apm-bin\bin\startup.bat
	SkyWalking管理 http://localhost:8080/
		生成配置文件
		dotnet tool install -g SkyAPM.DotNet.CLI
		dotnet skyapm config GeneralNetCore 127.0.0.1:11800
		
		设置环境变量
		On windows
			set ASPNETCORE_HOSTINGSTARTUPASSEMBLIES=SkyAPM.Agent.AspNetCore
			set SKYWALKING__SERVICENAME=GeneralNetCore
		On macOS/Linux
			export ASPNETCORE_HOSTINGSTARTUPASSEMBLIES=SkyAPM.Agent.AspNetCore
			export SKYWALKING__SERVICENAME=GeneralNetCore
			dotnet run
			
		
	
	