﻿using System;
using System.Collections.Generic;

namespace General.Entities
{
    public class DBConnectionOption
    {
        public string WriteConnection { get; set; }
        public List<string> ReadConnectionList { get; set; }
    }
}
