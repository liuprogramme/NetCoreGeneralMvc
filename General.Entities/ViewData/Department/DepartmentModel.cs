﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Entities.ViewData.Department
{
    public class DepartmentModel
    {
        public int? select { get; set; }
        public int? organizationId { get; set; }
        public int? parentId { get; set; }
        public string? organizationName { get; set; }
        public int? organizationType { get; set; }
        public int? sortNumber { get; set; }
        public string? createTime { get; set; }
        public string? updateTime { get; set; }
    }
}
