﻿using General.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Entities.ViewData.Article
{
    /// <summary>
    /// 文章详细信息用于网站前端渲染
    /// </summary>
    public class ArticleInfo : tc_article
    {
        public string cateName { get; set; }
        public string nickname { get; set; }
        public string userImage { get; set; }
        public SimpleArticleInfo lastModel { get; set; } //上一篇
        public SimpleArticleInfo nextModel { get; set; } //下一篇
    }
}
