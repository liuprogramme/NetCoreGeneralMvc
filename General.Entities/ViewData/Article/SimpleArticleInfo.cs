﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Entities.ViewData.Article
{
    public class SimpleArticleInfo
    {
        public int id { get; set; }
        public string title { get; set; }
        public string thumb { get; set; }

        public string username { get; set; }
        public string userImage { get; set; }
        public string cateName { get; set; }
        public int cateId { get; set; }

        public string creatTime { get; set; }
        public int hits { get; set; }
    }
}
