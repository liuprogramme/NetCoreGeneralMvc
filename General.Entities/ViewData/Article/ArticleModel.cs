﻿using General.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Entities.ViewData.Article
{
    public class ArticleModel:tc_article
    {
        public string cateName { get; set; }
        public string nickname { get; set; }
    }
}
