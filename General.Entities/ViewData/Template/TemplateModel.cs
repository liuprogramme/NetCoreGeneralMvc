﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Entities.ViewData.Template
{
    /// <summary>
    /// 模板列表
    /// </summary>
    public class TemplateModel
    {
        public string name { get; set; }
        public bool isChecked { get; set; }
    }
}
