﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Entities.ViewData.Role
{
    /// <summary>
    /// 角色权限分配
    /// </summary>
    public class PermissionmModel
    {
        public int id { get; set; }
        public string title { get; set; }
        public string field { get; set; }
        public bool? ischecked { get; set; }
        public bool spread { get; set; } = true;

        public List<PermissionmModel> children { get; set; }
    }
}
