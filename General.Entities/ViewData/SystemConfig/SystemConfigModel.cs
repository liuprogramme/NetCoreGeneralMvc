﻿using System;
namespace General.Entities.ViewData.SystemConfig
{
    public class SystemConfigModel
    {
        public string key { get; set; }
        public string value { get; set; }
    }
}
