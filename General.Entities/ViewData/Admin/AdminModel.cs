﻿using System;
using System.Collections.Generic;
using General.Entities.Models;

namespace General.Entities.ViewData.Admin
{
    public class AdminModel:tc_admin
    {
        public List<tc_role> roles { get; set; }
        public string thumbnailPath { get; set; }
    }
}
