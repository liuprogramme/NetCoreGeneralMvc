using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace General.Entities.Models
{
    /// <summary>
    /// tc_link
    ///</summary>
    [Table("tc_link")]
    public class tc_link
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string url { get; set; }
        public int nofollow { get; set; }
        public int sort { get; set; }
        public int status { get; set; }
        public DateTime createTime { get; set; }
        public DateTime updateTime { get; set; }

    }
}
