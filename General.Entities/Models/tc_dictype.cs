using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace General.Entities.Models
{
    /// <summary>
    /// 字典类别表
    /// </summary>
    [Table("tc_dictype")]
    public class tc_dictype
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public DateTime createtime { get; set; }
    }
}
