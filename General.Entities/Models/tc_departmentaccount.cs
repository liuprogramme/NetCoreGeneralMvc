using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace General.Entities.Models
{
    /// <summary>
    /// tc_departmentaccount
    ///</summary>
    [Table("tc_departmentaccount")]
    public class tc_departmentaccount
    {
        [Key]
        public int id { get; set; }
        public int departmentId { get; set; }
        public int uid { get; set; }
        public int cuser_id { get; set; }
        public DateTime createTime { get; set; }

    }
}
