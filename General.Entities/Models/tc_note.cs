﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace General.Entities.Models
{
    [Table("tc_note")]
    public class tc_note
    {
        [Key]
        public int id { get; set; }
        public string message { get; set; }
        public int uid { get; set; }
        public DateTime intime { get; set; }
    }
}
