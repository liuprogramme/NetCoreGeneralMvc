using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace General.Entities.Models
{
    /// <summary>
    /// tc_slider
    ///</summary>
    [Table("tc_slider")]
    public class tc_slider
    {
        [Key]
        public int id { get; set; }
        public string title { get; set; }
        public string? img { get; set; }
        public string? url { get; set; }
        public int sort { get; set; }
        public int status { get; set; }
        public DateTime createTime { get; set; }
        public DateTime updateTime { get; set; }

    }
}
