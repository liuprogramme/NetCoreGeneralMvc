using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace General.Entities.Models
{
    /// <summary>
    /// tc_diypage
    ///</summary>
    [Table("tc_diypage")]
    public class tc_diypage
    {
        [Key]
        public int id { get; set; }
        public string route { get; set; }
        public string title { get; set; }
        public string keywords { get; set; }
        public string description { get; set; }
        public string htmlContent { get; set; }
        public int isMaster { get; set; }
        public int status { get; set; }
        public DateTime createTime { get; set; }
        public DateTime updateTime { get; set; }

    }
}
