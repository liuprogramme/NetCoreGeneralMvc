using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace General.Entities.Models
{
    /// <summary>
    /// tc_authority
    ///</summary>
    [Table("tc_authority")]
    public class tc_authority
    {
        [Key]
        public int? id { get; set; }
        public string authorityName { get; set; }
        public string authority { get; set; }
        public int? isMenu { get; set; }
        public string menuIcon { get; set; }
        public string menuUrl { get; set; }
        public int? orderNumber { get; set; }
        public int? parentId { get; set; }
        public int isUse { get; set; }
        public DateTime createTime { get; set; }
        public DateTime updateTime { get; set; }

    }
}
