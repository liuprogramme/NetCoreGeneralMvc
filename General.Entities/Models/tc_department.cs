using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace General.Entities.Models
{
    /// <summary>
    /// tc_department
    ///</summary>
    [Table("tc_department")]
    public class tc_department
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string level { get; set; }
        public int parent_id { get; set; }
        public int seq { get; set; }
        public int organizationType { get; set; }
        public DateTime updateTime { get; set; }
        public DateTime createTime { get; set; }
        public int muser_id { get; set; }
        public int cuser_id { get; set; }

    }
}
