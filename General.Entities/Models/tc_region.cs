﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace General.Entities.Models
{
    [Table("tc_region")]
    public class tc_region
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public int pid { get; set; }
        public string sname { get; set; }
        public int level { get; set; }
        public string citycode { get; set; }
        public string yzcode { get; set; }
        public string mername { get; set; }
        public float Lng { get; set; }
        public float Lat { get; set; }
        public string pinyin { get; set; }
    }
}
