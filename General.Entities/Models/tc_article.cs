using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace General.Entities.Models
{
    /// <summary>
    /// tc_article
    ///</summary>
    [Table("tc_article")]
    public class tc_article
    {
        [Key]
        public int id { get; set; }
        public int cateId { get; set; }
        public string title { get; set; }
        public string subhead { get; set; }
        public string intro { get; set; }
        public string content { get; set; }
        public int author { get; set; }
        public string copyfrom { get; set; }
        public string keyWords { get; set; }
        public string description { get; set; }
        public int hits { get; set; }
        public int ontop { get; set; }
        public int iselite { get; set; }
        public string thumb { get; set; }
        public int status { get; set; }
        public DateTime createTime { get; set; }
        public DateTime updateTime { get; set; }

    }
}
