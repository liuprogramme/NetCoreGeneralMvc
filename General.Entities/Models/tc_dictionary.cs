﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace General.Entities.Models
{
    [Table("tc_dictionary")]
    public class tc_dictionary
    {
       [Key]
        public int id { get; set; }
        public int typeid { get; set; }
        public string dicname { get; set; }
        public string dicode { get; set; }
        public string remark { get; set; }
        public int sort { get; set; }
        public DateTime createtime { get; set; }
    }
}
