﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace General.Entities.Models
{
    [Table("tc_sysconfig")]
    public class tc_sysconfig
    {
        [Key]
        public int id { get; set; }
        public int tid { get; set; }
        //1:text 2:textarea 3:switch 4:file
        public int type { get; set; }
        //是否必填
        public int required { get; set; }
        //设置名称
        public string title { get; set; }
        //配置标识 用于函数调用，只能使用英文且不能重复
        public string code { get; set; }
        //说明
        public string describe { get; set; }
        public string value { get; set; }
        public int sort { get; set; }
        public DateTime createtime { get; set; }
    }
}
