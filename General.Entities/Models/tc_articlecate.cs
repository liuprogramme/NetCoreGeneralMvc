using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace General.Entities.Models
{
    /// <summary>
    /// tc_articlecate
    ///</summary>
    [Table("tc_articlecate")]
    public class tc_articlecate
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string keywords { get; set; }
        public string description { get; set; }
        public string image { get; set; }
        public int sort { get; set; }
        public int count { get; set; }
        public int status { get; set; }
        public DateTime createTime { get; set; }
        public DateTime updateTime { get; set; }

    }
}
