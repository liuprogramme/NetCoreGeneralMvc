using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace General.Entities.Models
{
    /// <summary>
    /// tc_navigation
    ///</summary>
    [Table("tc_navigation")]
    public class tc_navigation
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public string menuUrl { get; set; }
        public int? orderNumber { get; set; }
        public int? parentId { get; set; }
        public int isUse { get; set; }
        public DateTime createTime { get; set; }
        public DateTime updateTime { get; set; }

    }
}
