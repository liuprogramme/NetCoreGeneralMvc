﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace General.Entities.Models
{
    [Table("tc_role")]
    public class tc_role
    {
        [Key]
        public int id { get; set; }
        public string rolename { get; set; }
        public string rolecode { get; set; }
        public string permission { get; set; }
        public string remark { get; set; }
        public DateTime createtime { get; set; }
        public DateTime updatetime { get; set; }
    }
}
