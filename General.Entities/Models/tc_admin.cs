using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace General.Entities.Models
{
    /// <summary>
    /// 系统管理员表
    /// </summary>
    [Table("tc_admin")]
    public class tc_admin
    {
        [Key]
        public int id { get; set; }
        public string username { get; set; }
        public string password { get; set; }
        public string nickname { get; set; }
        public string role { get; set; }
        public int status { get; set; }
        public DateTime createtime { get; set; }
        public DateTime lasttime { get; set; }
        public string lastip { get; set; }
        public string headimg { get; set; }
        public string wechatOpenId { get; set; }
        public string qqOpenId { get; set; }
    }
}
