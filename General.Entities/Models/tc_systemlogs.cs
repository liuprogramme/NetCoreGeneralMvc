﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace General.Entities.Models
{
    /// <summary>
    /// 系统日志
    /// </summary>
    [Table("tc_systemlogs")]
    public class tc_systemlogs
    {
        [Key]
        public int id { get; set; }
        public string Timestamp { get; set; }
        public string Level { get; set; }
        public string Message { get; set; }
        public string Exception { get; set; }
    }
}
