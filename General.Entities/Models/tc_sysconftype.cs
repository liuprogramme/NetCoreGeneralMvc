﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace General.Entities.Models
{
    /// <summary>
    /// 系统设置类别
    /// </summary>
    [Table("tc_sysconftype")]
    public class tc_sysconftype
    {
        [Key]
        public int id { get; set; }
        public string name { get; set; }
        public int sort { get; set; }
        public DateTime createtime { get; set; }
    }
}
