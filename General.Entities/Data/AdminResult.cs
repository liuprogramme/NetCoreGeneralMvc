﻿using System;
namespace General.Entities.Data
{
    /// <summary>
    /// 后台表格渲染接口
    /// </summary>
    public class AdminResult
    {
        public int code { get; set; }
        public int count { get; set; }
        public object data { get; set; }
        public string msg { get; set; }

    }
}
