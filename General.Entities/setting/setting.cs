﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace General.Entities.setting
{
    [Table("setting")]
    public class setting
    {
        [Key]
        public int id { get; set; }
        public string key { get; set; }
        public int value { get; set; }
    }
}
