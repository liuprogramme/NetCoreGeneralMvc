using System;
using System.Collections.Generic;
using System.Text;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Options;

namespace General.Entities
{
    public class GeneralDbContext : DbContext
    {
        //public GeneralDbContext(DbContextOptions options) : base(options)
        //{

        //}

        private DBConnectionOption _readAndWrite = null;
        public GeneralDbContext(IOptionsMonitor<DBConnectionOption> options)
        {
            this._readAndWrite = options.CurrentValue;
            if (this._readAndWrite.ReadConnectionList == null)
            {
                this._readAndWrite.ReadConnectionList = new List<string>();
                this._readAndWrite.ReadConnectionList.Add(this._readAndWrite.WriteConnection);
            }
        }

        private static int _iSeed = 0;
        public DbContext ToRead()
        {
            //int num = new Random(_iSeed++).Next(0, this._readAndWrite.ReadConnectionList.Count);
            if (_iSeed > 9999999)
            {
                _iSeed = 0;
            }
            //轮训
            this.Database.GetDbConnection().ConnectionString = this._readAndWrite.ReadConnectionList[_iSeed++ % this._readAndWrite.ReadConnectionList.Count];
            return this;
        }
        public DbContext ToWrite()
        {
            this.Database.GetDbConnection().ConnectionString = this._readAndWrite.WriteConnection;
            return this;
        }

        public DbSet<setting.setting> settings { get; set; }
        public DbSet<Models.tc_systemlogs> tc_systemlogs { get; set; }
        public DbSet<Models.tc_admin> tc_admin { get; set; }
        public DbSet<Models.tc_note> tc_note { get; set; }
        public DbSet<Models.tc_role> tc_role { get; set; }
        public DbSet<Models.tc_dictype> tc_dictype { get; set; }
        public DbSet<Models.tc_dictionary> tc_dictionary { get; set; }
        public DbSet<Models.tc_region> tc_region { get; set; }
        public DbSet<Models.tc_sysconftype> tc_sysconftype { get; set; }
        public DbSet<Models.tc_sysconfig> tc_sysconfig { get; set; }
        //{CodeGeneration}
        public DbSet<Models.tc_departmentaccount> tc_departmentaccount { get; set; }
        public DbSet<Models.tc_department> tc_department { get; set; }
        public DbSet<Models.tc_link> tc_link { get; set; }
        public DbSet<Models.tc_article> tc_article { get; set; }
        public DbSet<Models.tc_articlecate> tc_articlecate { get; set; }
        public DbSet<Models.tc_diypage> tc_diypage { get; set; }
        public DbSet<Models.tc_slider> tc_slider { get; set; }
        public DbSet<Models.tc_navigation> tc_navigation { get; set; }
        public DbSet<Models.tc_authority> tc_authority { get; set; }


        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //切换数据库
            optionsBuilder.UseMySql(this._readAndWrite.WriteConnection, Microsoft.EntityFrameworkCore.ServerVersion.AutoDetect(this._readAndWrite.WriteConnection));
        }
    }
}
