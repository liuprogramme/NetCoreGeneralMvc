﻿using System;
using System.Collections.Generic;
using System.Text;

namespace General.Entities.Dapper
{
    /// <summary>
    /// 数据库类型
    /// </summary>
    public enum DbType
    {
        Access,
        SqlServer,
        Oracle,
        MySql,
        SqlLite
    }
}
