﻿using System;
namespace General.Framework
{
    /// <summary>
    /// 接口返回状态码
    /// </summary>
    public enum AjaxResultStateEnum
    {
        Error = 0,
        Ok = 1,
        Fail = 2
    }
}
