﻿using System;
using System.Collections.Generic;

namespace General.Framework.Admin
{
    /// <summary>
    /// 定时任务列表
    /// </summary>
    public static class JobListModel
    {
        public static List<JobItemModel> list =new List<JobItemModel>();
        public static int Increment = 1;
    }

    public class JobItemModel
    {
        public int id { get; set; }
        public string name { get; set; }
        public string trigger { get; set; }
        public string remark { get; set; }
        public DateTime createtime { get; set; }
    }

}
