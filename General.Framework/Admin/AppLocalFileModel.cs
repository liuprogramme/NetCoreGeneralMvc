﻿using System;
namespace General.Framework.Admin
{
    public class AppLocalFileModel
    {
        /// <summary>
        /// 是否有缩略图
        /// </summary>
        public bool hasSm { get; set; }

        /// <summary>
        /// 是否为目录
        /// </summary>
        public bool isDir { get; set; }

        /// <summary>
        /// 名称
        /// </summary>
        public string name { get; set; }

        /// <summary>
        /// 缩略图地址   
        /// </summary>
        public string smUrl { get; set; }

        /// <summary>
        /// 类型
        /// </summary>
        public string type { get; set; }

        /// <summary>
        /// 下载地址
        /// </summary>
        public string url { get; set; }


    }
}
