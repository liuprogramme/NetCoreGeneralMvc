﻿using System;
namespace General.Framework.Admin
{
    /// <summary>
    /// 本地便签Model
    /// </summary>
    public class AdminNoteModel
    {
        public int id { get; set; }
        public string time { get; set; }
        public string content { get; set; }
    }
}
