﻿using System;
using System.Collections.Generic;
using System.Text;

namespace General.Framework
{
    public class AjaxResult
    {
        public AjaxResultStateEnum code { get; set; }
        public object data { get; set; }
        public string message { get; set; }


        public string Ok(string message)
        {
            this.code = AjaxResultStateEnum.Ok;
            this.message = message;
            return JsonHelper.Serialize(this);
        }


        public string Error(string message)
        {
            this.code = AjaxResultStateEnum.Error;
            this.message = message;
            return JsonHelper.Serialize(this);
        }

        public string Fail(string message)
        {
            this.code = AjaxResultStateEnum.Fail;
            this.message = message;
            return JsonHelper.Serialize(this);
        }


      
    }


}
