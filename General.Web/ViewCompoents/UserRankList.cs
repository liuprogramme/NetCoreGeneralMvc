﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.ViewCompoents
{

    [ViewComponent(Name = "UserRankList")]
    public class UserRankList : ViewComponent
    {
        private IMemoryCache _memoryCache;
        private string cacheKey = "topicrank";

        public UserRankList(IMemoryCache memoryCache)
        {
            
            _memoryCache = memoryCache;
        }

        public IViewComponentResult Invoke(int days)
        {
            var items = new List<User>();
            if (!_memoryCache.TryGetValue(cacheKey, out items))
            {
                items = GetRankUsers(10, days);
            }
            _memoryCache.Set(cacheKey, items, TimeSpan.FromMinutes(10));
            return View(items);
        }

        private List<User> GetRankUsers(int top, int days)
        {
            List<User> list = new List<User>();
            list.Add(new User() { id = 1, name = "张三" });
            list.Add(new User() { id = 1, name = "李四" });
            return list;
        }


    }

    public class User
    {
        public int id { get; set; }
        public string name { get; set; }
    }

}
