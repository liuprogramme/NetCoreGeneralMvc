﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Serilog;
using Serilog.Events;

namespace General.Mvc
{
    public class Program
    {
        public static int Main(string[] args)
        {

            //var outputTemplate = "{Timestamp:HH:mm:ss.fff} [{Level:u3}] {SourceContext}{NewLine}{Message}{NewLine}{Exception}";

            //Log.Logger = new LoggerConfiguration()
            //   .MinimumLevel.Information()
            //   .MinimumLevel.Override("Microsoft", LogEventLevel.Error)
            //   .Enrich.FromLogContext()
            //   .WriteTo.Console(LogEventLevel.Information, outputTemplate: outputTemplate)
            //   .WriteTo.File("Logs/log.txt", rollingInterval: RollingInterval.Hour)
            //   .CreateLogger();

            Log.Logger = new LoggerConfiguration()
                   .Enrich.FromLogContext()
                   .WriteTo.Console()
                   .ReadFrom.Configuration(new ConfigurationBuilder()
                   .SetBasePath(Directory.GetCurrentDirectory())
                   .AddCommandLine(args)
                   .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                   .Build())
                   .CreateLogger();
            try
            {
                Log.Information("Starting web host");
                CreateWebHostBuilder(args).Build().Run();
                return 0;
            }
            catch (Exception ex)
            {
                Log.Fatal(ex, "Host terminated unexpectedly");
                return 1;
            }
            finally
            {
                Log.CloseAndFlush();
            }

            //new ConfigurationBuilder()
            //    .SetBasePath(Directory.GetCurrentDirectory())
            //    .AddCommandLine(args) //支持命令行参数
            //    .Build();


            //CreateWebHostBuilder(args).Build().Run();
        }

        public static IWebHostBuilder CreateWebHostBuilder(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseUrls("http://*:555")  
                .UseSerilog()
                .UseStartup<Startup>();
    }
}
