﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using General.Mvc.Jobs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Quartz;

namespace General.Mvc.Controllers.API
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class QuartzController : ControllerBase
    {
        private readonly ISchedulerFactory _schedulerFactory;
        private IScheduler _scheduler;

        public QuartzController(ISchedulerFactory schedulerFactory)
        {
            _schedulerFactory = schedulerFactory;
        }

        //每20秒 ： 0/20 * * * * ? 
        //0 0 10,14,16 * * ? 每天上午10点，下午2点，4点整触发
        //0 0/30 9-17 * * ?   朝九晚五工作时间内每半小时触发
        //0 0 12 ? * WED 表示每个星期三中午12点 触发
        //"0 0 12 * * ?" 每天中午12点触发 
        //"0 15 10 ? * *" 每天上午10:15触发 
        //"0 15 10 * * ?" 每天上午10:15触发 
        //"0 15 10 * * ? *" 每天上午10:15触发 
        //"0 15 10 * * ? 2005" 2005年的每天上午10:15触发 
        //"0 * 14 * * ?" 在每天下午2点到下午2:59期间的每1分钟触发 
        //"0 0/5 14 * * ?" 在每天下午2点到下午2:55期间的每5分钟触发 
        //"0 0/5 14,18 * * ?" 在每天下午2点到2:55期间和下午6点到6:55期间的每5分钟触发 
        //"0 0-5 14 * * ?" 在每天下午2点到下午2:05期间的每1分钟触发 
        //"0 10,44 14 ? 3 WED" 每年三月的星期三的下午2:10和2:44触发 
        //"0 15 10 ? * MON-FRI" 周一至周五的上午10:15触发 
        //"0 15 10 15 * ?" 每月15日上午10:15触发 
        //"0 15 10 L * ?" 每月最后一日的上午10:15触发 
        //"0 15 10 ? * 6L" 每月的最后一个星期五上午10:15触发 
        //"0 15 10 ? * 6L 2002-2005" 2002年至2005年的每月的最后一个星期五上午10:15触发 
        //"0 15 10 ? * 6#3" 每月的第三个星期五上午10:15触发

        /// <summary>
        /// 使用无参数的
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<string[]> Get()
        {
            //1、通过调度工厂获得调度器
            _scheduler = await _schedulerFactory.GetScheduler();
            //2、开启调度器
            await _scheduler.Start();
            //3、创建一个触发器
            var trigger = TriggerBuilder.Create()
                            .WithSimpleSchedule(x => x.WithIntervalInSeconds(5).RepeatForever())//每两秒执行一次
                            .Build();
            //4、创建任务
            var jobDetail = JobBuilder.Create<MyJob>()
                            //.WithIdentity("job", "group")
                            .Build();
            //5、将触发器和任务器绑定到调度器中
            await _scheduler.ScheduleJob(jobDetail, trigger);
            return await Task.FromResult(new string[] { "value1", "value2" });
        }

        /// <summary>
        /// 调用有参数的
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<string[]> GetPar()
        {
            //1、通过调度工厂获得调度器
            _scheduler = await _schedulerFactory.GetScheduler();
            //2、开启调度器
            await _scheduler.Start();
            //3、创建一个触发器


            var trigger = TriggerBuilder.Create()
                         //.WithSimpleSchedule(x => x.WithIntervalInSeconds(2).RepeatForever())//间隔2秒 一直执行
                         .WithCronSchedule("30,59 * * * * ?")//每分钟执行2次
                         //"0 0 0 1 1 ?" 每年元旦1月1日 0 点触发
                         //"0 15 10 * * ? *"         每天上午10:15触发  
                         //"0 0-5 14 * * ?"          每天下午2点到下午2: 05期间的每1分钟触发
                          .Build();

            //4、创建任务
            var jobDetail = JobBuilder.Create<MyJob>()
                                .UsingJobData("key1", 123)//通过Job添加参数值
                                .UsingJobData("key2", "123")
                                .WithIdentity("job1", "group1")
                            .Build();

            //IJobDetail job = JobBuilder.Create<MyJob>()
            //                    .UsingJobData("key1", 123)//通过Job添加参数值
            //                    .UsingJobData("key2", "123")
            //                    .WithIdentity("job1", "group1")
            //                    .Build();
            //5、将触发器和任务器绑定到调度器中
            await _scheduler.ScheduleJob(jobDetail, trigger);
            return await Task.FromResult(new string[] { "value1", "value2" });
        }




    }
}