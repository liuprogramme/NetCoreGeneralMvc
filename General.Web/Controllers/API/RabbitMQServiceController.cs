﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using EasyNetQ;
using General.Mvc.Models.RabbitMQ;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace General.Mvc.Controllers.API
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class RabbitMQServiceController : ControllerBase
    {
        private readonly IBus bus;

        public RabbitMQServiceController(IBus bus)
        {
            this.bus = bus;
        }

        [HttpGet]
        public async Task<string> SendMessage() 
        {
            Random r = new Random();
            ClientMessage message = new ClientMessage
            {
                ClientId = 1,
                ClientName = "hacker" + r.Next(1, 9999),
                Sex = "男",
                Age = 29,
                SmokerCode = "N",
                Education = "Master",
                YearIncome = 100000
            };
            await bus.PublishAsync(message);

            return "MQ发送消息成功~~~~";
        }


    }
}