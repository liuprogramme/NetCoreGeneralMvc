﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.Controllers.Web
{
    /// <summary>
    /// 错误页
    /// </summary>
    public class ErrorController : Controller
    {
        /// <summary>
        /// 404页面
        /// </summary>
        /// <returns></returns>
        [Route("/Error/404")]
        [HttpGet]
        public IActionResult NotFoundPage()
        {
            return View();
        }


        /// <summary>
        /// 500页面
        /// </summary>
        /// <returns></returns>
        [Route("/Error/500")]
        [HttpGet]
        public IActionResult InternalServerError()
        {
            return View();
        }

        /// <summary>
        /// 403页面 无权访问
        /// </summary>
        /// <returns></returns>
        [Route("/Error/403")]
        [HttpGet]
        public IActionResult NotPermissionAccess()
        {
            return View();
        }
    }
}
