﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using General.Core.Data;
using General.Entities;
using General.Entities.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace General.Mvc.Controllers.Web
{
    public class TestController : Controller
    {
        private readonly GeneralDbContext dbcontext;
        private readonly IRepository<tc_role> tcRoleRepository;

        public TestController(GeneralDbContext dbcontext, IRepository<tc_role> tcRoleRepository)
        {
            this.dbcontext = dbcontext;
            this.tcRoleRepository = tcRoleRepository;
        }



        // GET: /<controller>/
        [HttpGet]
        public IActionResult Index()
        {
            string conn = this.dbcontext.Database.GetDbConnection().ConnectionString;
            //return Ok(conn);
            return BadRequest("no auth");
        }



        [HttpGet]
        public IActionResult Get()
        {
            var model = tcRoleRepository.GetAllAsync();

            return Ok(model);
        }



    }
}
