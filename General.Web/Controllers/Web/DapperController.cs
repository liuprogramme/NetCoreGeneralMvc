﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using General.Entities.Dapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace General.Mvc.Controllers.Web
{
    [Route("api/[controller]/[action]")]
    [ApiController]
    public class DapperController : ControllerBase
    {
        private readonly DbRepository dbRepository;

        public DapperController()
        {
            //初始化dapper
            this.dbRepository = new DbRepository();
        }


        /// <summary>
        /// 测试
        /// </summary>
        /// <returns></returns>
        //[HttpPost] [HttpGet]
        //public IActionResult Test() 
        //{

        //    var data =  dbRepository.Query<Students>("select * from Students",new { id=1 } );
            
        //    return Ok(data);
        //}


    }
}