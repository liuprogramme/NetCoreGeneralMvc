﻿using General.Mvc.Filters;
using General.Services.DiyPage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.Controllers.Web
{
    public class DiyPageController : Controller
    {
        private readonly IDiyPageService diyPageService;

        public DiyPageController(IDiyPageService diyPageService)
        {
            this.diyPageService = diyPageService;
        }

        /// <summary>
        /// 自定义页面
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("/Page/{id}")]
        [Route("/Page/{id}.html")]
        [ServiceFilter(typeof(PageTemplateFilter))]
        public async Task<IActionResult> Index(string id)
        {
            var result = await diyPageService.GetPageHtml(id);
            HttpContext.Response.ContentType = "text/html;charset=utf-8";
            return result;
        }
    }
}
