﻿using General.Core.Config;
using General.Mvc.Filters;
using General.Services.Template;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.Controllers.Web
{
    public class IndexController : Controller
    {
        private readonly ITemplateService templateService;

        public  IndexController(ITemplateService templateService)
        {
            this.templateService = templateService;
        }



        /// <summary>
        /// 首页
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Route("Index/Main")]
        [Route("index.html")]
        [Route("/")]
        [ServiceFilter(typeof(PageTemplateFilter))]
        //[Route("Index/Main/{id}/{parm}")]
        public async Task<IActionResult> Main()
        {
            var result = await templateService.GetPageHtmlContent(WebSiteTemplateConfig.Site_Template_Main_Info.name);
            HttpContext.Response.ContentType = "text/html";
            return Content(result);
        }
    }
}
