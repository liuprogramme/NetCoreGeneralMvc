﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;

namespace General.Mvc.Controllers.Web
{
    public class ImagesController : Controller
    {
        private readonly IWebHostEnvironment webHostEnvironment;

        public ImagesController(IWebHostEnvironment webHostEnvironment)
        {
            this.webHostEnvironment = webHostEnvironment;
        }


        /// <summary>
        /// 缩略图获取接口
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        //[Route("/images/{name}")]
        public async Task<IActionResult> GetPath(string name="", int width = 100)
        {
            if (name.StartsWith("/upload"))
            {
                name = name.Replace("/upload","");
            }
            var appPath = Path.Combine(webHostEnvironment.WebRootPath + "/upload");
            var errorImage = Path.Combine(appPath, "404.png");//没有找到图片
            var imgPath = string.IsNullOrEmpty(name) ? errorImage : Path.Combine(appPath + name);
            //获取图片的返回类型
            var contentTypDict = new Dictionary<string, string> {
                {"jpg","image/jpeg"},
                {"jpeg","image/jpeg"},
                {"jpe","image/jpeg"},
                {"png","image/png"},
                {"gif","image/gif"},
                {"ico","image/x-ico"},
                {"tif","image/tiff"},
                {"tiff","image/tiff"},
                {"fax","image/fax"},
                {"wbmp","image//vnd.wap.wbmp"},
                {"rp","image/vnd.rn-realpix"}
            };
            var contentTypeStr = "image/jpeg";
            var imgTypeSplit = name.Split('.');
            var imgType = imgTypeSplit[imgTypeSplit.Length - 1].ToLower();
            //未知的图片类型
            if (!contentTypDict.ContainsKey(imgType))
            {
                imgPath = errorImage;
            }
            else
            {
                contentTypeStr = contentTypDict[imgType];
            }
            if (!new FileInfo(imgPath).Exists)//图片不存在
            {
                imgPath = errorImage;
            }
            //原图
            if (width <= 0)
            {
                using (var sw = new FileStream(imgPath, FileMode.Open))
                {
                    var bytes = new byte[sw.Length];
                    await sw.ReadAsync(bytes, 0, bytes.Length);
                    sw.Close();
                    return new FileContentResult(bytes, contentTypeStr);
                }
            }
            //缩小图片
            byte[] imageBytes = await System.IO.File.ReadAllBytesAsync(imgPath);
            Image img = Image.FromStream(new MemoryStream(imageBytes));
            Size newSize = ResizeImage(img.Width, img.Height, width, width);
            using (Image displayImage = new Bitmap(img, newSize))
            {
                try
                {
                    //displayImage.Save(newFileName, original.RawFormat);
                    var ms = new MemoryStream();
                    displayImage.Save(ms, ImageFormat.Bmp);
                    var bytes = ms.GetBuffer();
                    ms.Close();
                    return new FileContentResult(bytes, contentTypeStr);
                }
                finally
                {
                    img.Dispose();
                }
            }

        }


        /// <summary>
        /// 计算新尺寸
        /// </summary>
        /// <param name="width">原始宽度</param>
        /// <param name="height">原始高度</param>
        /// <param name="maxWidth">最大新宽度</param>
        /// <param name="maxHeight">最大新高度</param>
        /// <returns></returns>
        private static Size ResizeImage(int width, int height, int maxWidth, int maxHeight)
        {
            if (maxWidth <= 0)
                maxWidth = width;
            if (maxHeight <= 0)
                maxHeight = height;
            decimal MAX_WIDTH = maxWidth;
            decimal MAX_HEIGHT = maxHeight;
            decimal ASPECT_RATIO = MAX_WIDTH / MAX_HEIGHT;

            int newWidth, newHeight;
            decimal originalWidth = width;
            decimal originalHeight = height;

            if (originalWidth > MAX_WIDTH || originalHeight > MAX_HEIGHT)
            {
                decimal factor;
                if (originalWidth / originalHeight > ASPECT_RATIO)
                {
                    factor = originalWidth / MAX_WIDTH;
                    newWidth = Convert.ToInt32(originalWidth / factor);
                    newHeight = Convert.ToInt32(originalHeight / factor);
                }
                else
                {
                    factor = originalHeight / MAX_HEIGHT;
                    newWidth = Convert.ToInt32(originalWidth / factor);
                    newHeight = Convert.ToInt32(originalHeight / factor);
                }
            }
            else
            {
                newWidth = width;
                newHeight = height;
            }
            return new Size(newWidth, newHeight);
        }

    }
}
