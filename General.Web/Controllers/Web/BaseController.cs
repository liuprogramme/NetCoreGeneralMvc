﻿using General.Services.Template;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.Controllers.Web
{
    public class BaseController : Controller
    {
        private readonly ITemplateService templateService;

        public BaseController(ITemplateService templateService)
        {
            this.templateService = templateService;

            GetTemplatePages();
        }

        /// <summary>
        /// 获取模板html内容加载到内存
        /// </summary>
        /// <returns></returns>
        public async Task GetTemplatePages()
        {
            await templateService.GetTemplateHtmlPages();
        }


    }
}
