﻿using General.Entities.Models;
using General.Framework;
using General.Mvc.Filters;
using General.Services.Role;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.Controllers.Admin
{
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    [ServiceFilter(typeof(ResponseLogTimeFilter))]
    public class RoleController : ControllerBase
    {
        private readonly IRoleService roleService;

        public RoleController(IRoleService roleService)
        {
            this.roleService = roleService;
        }

       

        #region 角色管理
        /// <summary>
        /// 获取所有角色
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> GetAllRole()
        {
            var list = await roleService.GetAllList();
            return Content(JsonHelper.Serialize(list));
        }
        /// <summary>
        /// 查询角色列表
        /// </summary>
        [HttpGet]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> GetRoleList(int page, int limit, string rolename, string rolecode, string remark)
        {
            var list = await roleService.GetList(page, limit, rolename, rolecode, remark);
            return Content(JsonHelper.Serialize(list));
        }
        /// <summary>
        /// 增加角色信息
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> AddRoleInfo([FromForm] tc_role role)
        {
            await roleService.AddInfo(role);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "添加成功" }));
        }
        /// <summary>
        /// 修改角色信息
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> EditRoleInfo([FromForm] tc_role role)
        {
            await roleService.EditInfo(role.id, role);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "修改成功" }));
        }
        /// <summary>
        /// 删除角色信息
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> DeleteRoleInfo([FromForm] string ids)
        {
            var res = await roleService.DeleteInfo(ids);
            if (res > 0)
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "删除成功" }));
            }
            else
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Fail, message = "出现错误" }));
            }
        }
        /// <summary>
        /// 获取角色权限列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> GetPermissionList([FromQuery]int id)
        {
            var data = JsonHelper.Serialize(await roleService.GetPermissionmData(id));
            return Content(data);
        }

        /// <summary>
        /// 修改角色权限信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> EditPermissionInfo([FromForm] int id, [FromForm] string data="")
        {
            await roleService.EditPermissionInfo(id, data);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "修改成功" }));
        }
        #endregion

    }
}
