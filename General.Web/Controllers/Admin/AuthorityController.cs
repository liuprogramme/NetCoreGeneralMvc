﻿using General.Entities.ViewData.Authority;
using General.Framework;
using General.Mvc.Filters;
using General.Services.Authority;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.Controllers.Admin
{
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    [ServiceFilter(typeof(ResponseLogTimeFilter))]
    public class AuthorityController : Controller
    {
        private readonly IAuthorityService authorityService;

        public AuthorityController(IAuthorityService authorityService)
        {
            this.authorityService = authorityService;
        }

        /// <summary>
        /// 查询权限菜单列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> GetAuthorityList(string authorityName, string menuUrl, string authority)
        {
            var list = await authorityService.GetList(authorityName, menuUrl, authority);
            return Content(JsonHelper.Serialize(list));
        }

        /// <summary>
        /// 增加权限菜单
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> AddAuthority([FromForm] AuthorityModel model)
        {
            //判断上级是否为菜单才能添加，按钮不可添加
            if(!await authorityService.IsMenuById(Convert.ToInt32(model.parentId)))
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Error, message = "上级非菜单，不可添加!" }));
            }
            if (model.isMenu == 1)
            {
                if (string.IsNullOrWhiteSpace(model.authority))
                {
                    return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Error, message = "当前权限类型为按钮时权限标识必须填写!" }));
                }
            }
            
            
            if(model.isMenu == 1 || (model.isMenu ==0 && !string.IsNullOrWhiteSpace(model.authority)))
            {
                if (await authorityService.IsHasAuthority(model.authority))
                {
                    return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Error, message = "标识已存在，请换个标识名称!" }));
                }
            }

            

            await authorityService.AddInfo(model);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "增加成功!" }));
        }



        /// <summary>
        /// 编辑权限菜单
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> EditAuthority([FromForm] AuthorityModel model)
        {
            //判断上级是否为菜单才能添加，按钮不可添加
            if (!await authorityService.IsMenuById(Convert.ToInt32(model.parentId)))
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Error, message = "上级不能为按钮!" }));
            }
            if (model.id == model.parentId)
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Error, message = "上级不能为自己!" }));
            }
            if (model.isMenu == 1)
            {
                if (string.IsNullOrWhiteSpace(model.authority))
                {
                    return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Error, message = "当前权限类型为按钮时权限标识必须填写!" }));
                }
            }

            if (model.isMenu == 1 || (model.isMenu == 0 && !string.IsNullOrWhiteSpace(model.authority)))
            {
                if (await authorityService.IsHasAuthority(model.authority,model.id))
                {
                    return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Error, message = "标识已存在，请换个标识名称!" }));
                }
            }

            await authorityService.EditInfo(model);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "修改成功!" }));
        }


        /// <summary>
        /// 删除权限菜单
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> DeleteAuthority([FromForm] string ids)
        {
            var res = await authorityService.DeleteInfo(ids);
            if (res > 0)
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "删除成功！" }));
            }
            else
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Fail, message = "出现错误！" }));
            }
        }

    }
}
