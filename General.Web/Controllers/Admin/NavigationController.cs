﻿using General.Entities.Models;
using General.Framework;
using General.Mvc.Filters;
using General.Services.Navigation;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.Controllers.Admin
{
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    [ServiceFilter(typeof(ResponseLogTimeFilter))]
    public class NavigationController : ControllerBase
    {
        private readonly INavigationService navigationService;

        public NavigationController(INavigationService navigationService)
        {
            this.navigationService = navigationService;
        }


        /// <summary>
        /// 查询导航数据列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> GetNavigationList(string name, string menuUrl)
        {
            var list = await navigationService.GetList(name, menuUrl);
            return Content(JsonHelper.Serialize(list));
        }


        /// <summary>
        /// 增加网站导航菜单
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> AddNavigation([FromForm] tc_navigation model)
        {
            await navigationService.AddInfo(model);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "增加成功!" }));
        }



        /// <summary>
        /// 编辑网站导航菜单
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> EditNavigation([FromForm] tc_navigation model)
        {
            if (model.id == model.parentId)
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Error, message = "上级不能为自己!" }));
            }
            var res = await navigationService.EditInfo(model);
            if (res > 0)
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "修改成功!" }));

            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Fail, message = "出现错误！" }));
        }


        /// <summary>
        /// 删除网站导航菜单
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> DeleteNavigation([FromForm] string ids)
        {
            var res = await navigationService.DeleteInfo(ids);
            if (res > 0)
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "删除成功！" }));
            }
            else
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Fail, message = "出现错误！" }));
            }
        }
    }
}
