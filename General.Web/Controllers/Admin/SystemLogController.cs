﻿using General.Framework;
using General.Mvc.Filters;
using General.Services.SystemLogs;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.Controllers.Admin
{
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    public class SystemLogController : Controller
    {
        private readonly ISystemLogsService systemLogsService;

        public SystemLogController(ISystemLogsService systemLogsService)
        {
            this.systemLogsService = systemLogsService;
        }



        #region 系统日志
        /// <summary>
        /// 获取系统日志列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> GetSystemLog(int page, int limit, string message, string exception, string level)
        {
            var list = await systemLogsService.GetList(page, limit, message, exception, level);
            return Content(JsonHelper.Serialize(list));
        }
        /// <summary>
        /// 清空系统日志列表
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> DeleteAllSystemLog()
        {
            var res = await systemLogsService.DeleteAll();
            if (res > 0)
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "清空成功！" }));
            }
            else
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Fail, message = "已清空，无须再次清空！" }));
            }
        }

        /// <summary>
        /// 删除系统日志
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> DeleteSystemLogByIds([FromForm] string ids)
        {
            await systemLogsService.DeleteByIds(ids);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "删除成功！" }));
        }

        #endregion




    }
}
