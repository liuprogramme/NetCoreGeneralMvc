﻿using General.Entities.Models;
using General.Entities.ViewData.SystemConfig;
using General.Framework;
using General.Mvc.Filters;
using General.Services.SystemConfig;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.Controllers.Admin
{
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    [ServiceFilter(typeof(ResponseLogTimeFilter))]
    public class SystemConfigController : Controller
    {
        private readonly ISystemConfigService systemConfigService;

        public SystemConfigController(ISystemConfigService systemConfigService)
        {
            this.systemConfigService = systemConfigService;
        }

        #region 系统设置
        /// <summary>
        /// 添加系统配置信息
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> AddSystemConfig([FromForm] tc_sysconfig sysconfig)
        {
            AjaxResult result = new AjaxResult();
            var isExist = await systemConfigService.GetConfigInfoByCode(sysconfig.code);
            if (isExist != null)
            {
                return Content(result.Error("添加失败！已存在此配置标识！"));
            }
            var res = await systemConfigService.AddConfigData(sysconfig);
            if (res > 0)
            {
                return Content(result.Ok("添加成功"));
            }
            return Content(result.Error("添加失败"));
        }

        /// <summary>
        /// 编辑系统配置信息
        /// </summary>
        /// <param name="tid"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> EditSystemConfig([FromForm] int tid, [FromForm] string data)
        {
            AjaxResult result = new AjaxResult();
            var list = JsonHelper.Deserialize<List<SystemConfigModel>>(data);
            await systemConfigService.EditConfigData(tid, list);
            return Content(result.Ok("设置成功"));
        }

        #endregion
    }
}
