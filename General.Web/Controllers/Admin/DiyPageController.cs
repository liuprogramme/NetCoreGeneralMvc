﻿using General.Entities.Models;
using General.Framework;
using General.Mvc.Filters;
using General.Services.DiyPage;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.Controllers.Admin
{
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    [ServiceFilter(typeof(ResponseLogTimeFilter))]
    public class DiyPageController : Controller
    {
        private readonly IDiyPageService diyPageService;

        public DiyPageController(IDiyPageService diyPageService)
        {
            this.diyPageService = diyPageService;
        }


        /// <summary>
        /// 查询自定义页面列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> GetDiyPageList(int page, int limit, string name)
        {
            var list = await diyPageService.GetList(page,limit, name);
            return Content(JsonHelper.Serialize(list));
        }

        /// <summary>
        /// 增加自定义页面
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> AddDiyPage([FromForm] tc_diypage model)
        {
            if (await diyPageService.IsHasRoute(model.route))
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Error, message = "路由已存在，请换个路由名称!" }));
            }
            await diyPageService.AddInfo(model);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "增加成功!" }));
        }


        /// <summary>
        /// 编辑自定义页面
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> EditDiyPage([FromForm] tc_diypage model)
        {
            if (await diyPageService.IsHasRoute(model.route,model.id))
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Error, message = "路由已存在，请换个路由名称!" }));
            }
            var res = await diyPageService.EditInfo(model);
            if (res > 0)
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "修改成功!" }));

            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Fail, message = "出现错误！" }));
        }

        /// <summary>
        /// 删除自定义页面
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> DeleteDiyPage([FromForm] string ids)
        {
            var res = await diyPageService.DeleteInfo(ids);
            if (res > 0)
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "删除成功！" }));
            }
            else
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Fail, message = "出现错误！" }));
            }
        }

    }
}
