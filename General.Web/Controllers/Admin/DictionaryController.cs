﻿using General.Entities.Models;
using General.Framework;
using General.Mvc.Filters;
using General.Services.DicType;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.ComponentModel.Design;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.Controllers.Admin
{
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    [ServiceFilter(typeof(ResponseLogTimeFilter))]
    public class DictionaryController : Controller
    {
        private readonly IDicTypeService dicTypeService;
        private readonly Services.Dictionary.IDictionaryService dictionaryService;

        public DictionaryController(IDicTypeService dicTypeService,
            Services.Dictionary.IDictionaryService dictionaryService)
        {
            this.dicTypeService = dicTypeService;
            this.dictionaryService = dictionaryService;
        }



        #region 字典管理
        /// <summary>
        /// 查询字典类型列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> GetDicTypeList(int page, int limit, string name)
        {
            var list = await dicTypeService.GetList(page, limit, name);
            return Content(JsonHelper.Serialize(list));
        }
        /// <summary>
        /// 增加字典类型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> AddDicType([FromForm] tc_dictype dictype)
        {
            await dicTypeService.AddInfo(dictype);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "增加成功!" }));
        }
        /// <summary>
        /// 编辑字典类型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> EditDicType([FromForm] tc_dictype dictype)
        {
            await dicTypeService.EditInfo(dictype);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "编辑成功!" }));
        }
        /// <summary>
        /// 删除字典类型
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> DeleteDicType([FromForm] string ids)
        {
            var res = await dicTypeService.DeleteInfo(ids);
            if (res > 0)
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "删除成功" }));
            }
            else
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Fail, message = "出现错误" }));
            }
        }
        /// <summary>
        /// 获取字典列表
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> GetDictionaryList(int page, int limit, int typeid, string? dicname, string? dicode)
        {
            var list = await dictionaryService.GetList(page, limit, typeid, dicname, dicode);
            return Content(JsonHelper.Serialize(list));
        }
        /// <summary>
        /// 增加字典数据
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> AddDictInfo([FromForm] tc_dictionary dictionary)
        {
            var res = await dictionaryService.AddInfo(dictionary);
            if (res == 0)
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Fail, message = "已存在字典项名称!" }));
            }
            else
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "增加成功!" }));
            }
        }
        /// <summary>
        /// 编辑字典数据
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> EditDictInfo([FromForm] tc_dictionary dictionary)
        {
            await dictionaryService.EditInfo(dictionary);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "编辑成功!" }));
        }
        /// <summary>
        /// 删除字典数据
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> DeleteDictInfo([FromForm] string ids)
        {
            var res = await dictionaryService.DeleteInfo(ids);
            if (res > 0)
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "删除成功" }));
            }
            else
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Fail, message = "出现错误" }));
            }
        }
        #endregion



    }
}
