﻿using General.Entities.Models;
using General.Framework;
using General.Mvc.Filters;
using General.Services.Region;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.Controllers.Admin
{
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    [ServiceFilter(typeof(ResponseLogTimeFilter))]
    public class RegionController : Controller
    {
        private readonly IRegionService regionService;

        public RegionController(IRegionService regionService)
        {
            this.regionService = regionService;
        }



        #region 区域管理
        /// <summary>
        /// 获取区域列表
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> GetRegionList(int page, int limit, string name, int level)
        {
            var list = await regionService.GetList(page, limit, name, level);
            return Content(JsonHelper.Serialize(list));
        }
        /// <summary>
        /// 增加区域信息
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> AddRegionInfo([FromForm] tc_region data)
        {
            await regionService.AddInfo(data);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "添加成功" }));
        }
        /// <summary>
        /// 修改区域信息
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> EditRegionInfo([FromForm] tc_region data)
        {
            await regionService.EditInfo(data);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "修改成功" }));
        }
        /// <summary>
        /// 删除区域信息
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> DeleteRegionInfo([FromForm] string ids)
        {
            var res = await regionService.DeleteInfo(ids);
            if (res > 0)
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "删除成功" }));
            }
            else
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Fail, message = "出现错误" }));
            }
        }
        #endregion



    }
}
