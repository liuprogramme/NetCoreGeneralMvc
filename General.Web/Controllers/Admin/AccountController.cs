﻿using General.Entities.Models;
using General.Framework;
using General.Mvc.Filters;
using General.Services.Admin;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.Controllers.Admin
{
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    [ServiceFilter(typeof(ResponseLogTimeFilter))]
    public class AccountController : Controller
    {
        private readonly IAdminService adminService;

        public AccountController(IAdminService adminService)
        {
            this.adminService = adminService;
        }

        #region 管理员列表
        /// <summary>
        /// 获取管理员列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> GetAdminList(int page, int limit, string username, int status = -1,int organizationId = -1)
        {
            var list = await adminService.GetList(page, limit, username, status, organizationId);
            return Content(JsonHelper.Serialize(list));
        }

        /// <summary>
        /// 增加管理员
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> AddAdminInfo([FromForm] string username, [FromForm] int status, [FromForm] string role)
        {
            var result = new AjaxResult();
            if (string.IsNullOrWhiteSpace(username))
            {
                result.code = AjaxResultStateEnum.Error;
                result.message = "账号不能为空！";
                return Content(JsonHelper.Serialize(result));
            }
            var isExist = await adminService.GetInfoByUserName(username);
            if (isExist != null)
            {
                result.code = AjaxResultStateEnum.Error;
                result.message = "此用户名已存在！";
                return Content(JsonHelper.Serialize(result));
            }
            var res = await adminService.AddInfo(username, status, role);
            if (res > 0)
            {
                result.code = AjaxResultStateEnum.Ok;
                result.message = "添加成功！";
                return Content(JsonHelper.Serialize(result));
            }
            else
            {
                result.code = AjaxResultStateEnum.Error;
                result.message = "发生错误！";
                return Content(JsonHelper.Serialize(result));
            }

        }

        /// <summary>
        /// 编辑管理员
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> EditAdminInfo([FromForm] tc_admin data)
        {
            var result = new AjaxResult();
            if (string.IsNullOrWhiteSpace(data.username))
            {
                result.code = AjaxResultStateEnum.Error;
                result.message = "账号不能为空！";
                return Content(JsonHelper.Serialize(result));
            }
            var isExist = await adminService.GetInfoByUserName(data.username);
            if (isExist != null && isExist.id != data.id)
            {
                result.code = AjaxResultStateEnum.Error;
                result.message = "此用户名已存在！";
                return Content(JsonHelper.Serialize(result));
            }
            await adminService.EditInfo(data);
            result.code = AjaxResultStateEnum.Ok;
            result.message = "编辑成功！";
            return Content(JsonHelper.Serialize(result));
        }

        /// <summary>
        /// 删除管理员
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> DeleteAdminInfo([FromForm] string ids)
        {
            var res = await adminService.DeleteInfo(ids);
            if (res > 0)
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "删除成功！" }));
            }
            else
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Fail, message = "出现错误！" }));
            }
        }

        /// <summary>
        /// 修改管理员状态
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> EditAdminStatus([FromForm] int id, [FromForm] int status)
        {
            var res = await adminService.EditStatus(id, status);
            if (res > 0)
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "修改成功！" }));
            }
            else
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Fail, message = "出现错误！" }));
            }
        }


        /// <summary>
        /// 修改管理员密码
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> UpdateAdminPassword([FromForm] int uid, [FromForm] string password)
        {
            await adminService.UpdateUserPwd(uid, password);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "修改成功！" }));
        }

        #endregion


    }
}
