﻿using AutoMapper;
using General.Entities.Models;
using General.Framework;
using General.Framework.Admin;
using General.Mvc.Filters;
using General.Services.Note;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.Controllers.Admin
{
    /// <summary>
    /// 书签控制器
    /// </summary>
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    [ServiceFilter(typeof(ResponseLogTimeFilter))]
    public class NoteController : ControllerBase
    {
        private readonly INoteService noteService;
        private readonly IMapper mapper;

        public NoteController(INoteService noteService,
             IMapper mapper)
        {
            this.noteService = noteService;
            this.mapper = mapper;
        }

        #region 便签接口
        /// <summary>
        /// 获取所有本地便签
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> GetNoteList()
        {
            var res = await noteService.GetAllList();
            var list = mapper.Map<List<AdminNoteModel>>(res);
            return Content(JsonHelper.Serialize(list));
        }

        /// <summary>
        /// 添加便签
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> AddNote([FromForm] string message)
        {
            var user = JsonHelper.Deserialize<tc_admin>(HttpContext.Session.GetString("Admin"));
            await noteService.AddInfo(message, user.id);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "添加成功!" }));
        }

        /// <summary>
        /// 编辑便签
        /// </summary>
        /// <param name="message"></param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> EditNote([FromForm] string message, [FromForm] int id)
        {
            await noteService.UpdateInfo(id, message);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "编辑成功!" }));
        }

        /// <summary>
        /// 删除便签
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> DeleteNote([FromForm] int id)
        {
            await noteService.DeleteInfo(id);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "删除成功!" }));
        }
        #endregion

    }
}
