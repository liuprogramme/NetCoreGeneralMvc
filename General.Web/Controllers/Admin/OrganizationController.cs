﻿using General.Entities.Models;
using General.Entities.ViewData.Department;
using General.Framework;
using General.Mvc.Filters;
using General.Services.Department;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Web.Controllers.Admin
{
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    [ServiceFilter(typeof(ResponseLogTimeFilter))]
    public class OrganizationController : Controller
    {
        private readonly IDepartmentService departmentService;

        public OrganizationController(IDepartmentService departmentService)
        {
            this.departmentService = departmentService;
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> GetList()
        {
            var list = await departmentService.GetList();
            return Content(JsonHelper.Serialize(list));
        }

        /// <summary>
        /// 增加机构
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> AddData([FromForm]DepartmentModel model)
        {
            if (model.select != null)
            {
                var parentRes = await departmentService.GetDataByID(Convert.ToInt32(model.select));
                if (model.organizationType < parentRes.organizationType)
                {
                    return Content(new AjaxResult().Error("添加失败！机构类型不能比上级级别高！"));
                }
            }
            var user = JsonHelper.Deserialize<tc_admin>(HttpContext.Session.GetString("Admin"));
            await departmentService.AddInfo(model, user.id);
            
            return Content(new AjaxResult().Ok("添加成功！"));
        }


        /// <summary>
        /// 编辑机构
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> EditData([FromForm] DepartmentModel model)
        {
            if (model.select != null)
            {
                var parentRes = await departmentService.GetDataByID(Convert.ToInt32(model.select));
                if (model.organizationType < parentRes.organizationType)
                {
                    return Content(new AjaxResult().Error("编辑失败！机构类型不能比上级级别高！"));
                }
            }
            var user = JsonHelper.Deserialize<tc_admin>(HttpContext.Session.GetString("Admin"));
            var res = await departmentService.EditInfo(model, user.id);
            if (res > 0)
                return Content(new AjaxResult().Ok("添加成功！"));
            else
                return Content(new AjaxResult().Error("添加失败！"));
        }


        /// <summary>
        /// 删除机构
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> DeleteData([FromForm] int id)
        {
            var res = await departmentService.DeleteInfo(id);
            if(res>0)
                return Content(new AjaxResult().Ok("删除成功！"));
            else
                return Content(new AjaxResult().Error("删除失败！"));
        }

        /// <summary>
        /// 将用户添加到指定的机构
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> AddAccountDepartment([FromForm]int organizationId,[FromForm]string uid)
        {
            if (await departmentService.IsExistAccountDepartment(organizationId, uid))
            {
                return Content(new AjaxResult().Error("添加失败！选中用户已存在此部门！"));
            }
            var user = JsonHelper.Deserialize<tc_admin>(HttpContext.Session.GetString("Admin"));
            await departmentService.AddAccountDepartment(organizationId, uid, user.id);
            return Content(new AjaxResult().Ok("添加成功！"));
        }


        /// <summary>
        /// 删除机构所在的用户
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> DeleteAccountDepartment([FromForm] string ids)
        {
            var res = await departmentService.DeleteAccountDepartment(ids);
            if (res > 0)
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "删除成功！" }));
            }
            else
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Fail, message = "出现错误！" }));
            }
        }
    }
}
