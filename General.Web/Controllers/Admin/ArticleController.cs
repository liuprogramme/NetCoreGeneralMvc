﻿using General.Entities.Models;
using General.Framework;
using General.Mvc.Filters;
using General.Services.Article;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.Controllers.Admin
{
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    [ServiceFilter(typeof(ResponseLogTimeFilter))]
    public class ArticleController : Controller
    {
        private readonly IArticleService articleService;

        public ArticleController(IArticleService articleService)
        {
            this.articleService = articleService;
        }


        /// <summary>
        /// 查询文章列表
        /// </summary>
        [HttpGet]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> GetArticleList(string releaseDate, int page, int limit, string title, int cateId = -1, int status = -1, int ontop = -1, int iselite = -1)
        {
            DateTime? startTime = null;
            DateTime? endTime = null;
            if (!string.IsNullOrWhiteSpace(releaseDate))
            {
                var date = releaseDate.Split(" - ", StringSplitOptions.RemoveEmptyEntries);
                startTime = Convert.ToDateTime(date[0].Trim());
                endTime = Convert.ToDateTime(date[1].Trim());
            }
           
            var list = await articleService.GetList(startTime, endTime, page,limit, title, cateId, status, ontop, iselite);
            return Content(JsonHelper.Serialize(list));
        }


        /// <summary>
        /// 获取文章内容
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> GetArticleContent([FromForm] int id)
        {
            var result =  await articleService.GetArticleContent(id);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "获取成功!", data= result }));
        }


        /// <summary>
        /// 增加文章
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> AddArticleInfo([FromForm] tc_article model)
        {
            await articleService.AddInfo(model);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "增加成功!" }));
        }


        /// <summary>
        /// 编辑文章
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> EditArticleInfo([FromForm] tc_article model)
        {
            var res = await articleService.EditInfo(model);
            if (res > 0)
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "修改成功!" }));

            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Fail, message = "出现错误！" }));
        }


        /// <summary>
        /// 删除文章
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> DeleteArticle([FromForm] string ids)
        {
            var res = await articleService.DeleteInfo(ids);
            if (res > 0)
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "删除成功！" }));
            }
            else
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Fail, message = "出现错误！" }));
            }
        }

    }
}
