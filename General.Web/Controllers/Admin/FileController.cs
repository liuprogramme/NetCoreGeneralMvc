﻿using General.Framework;
using General.Framework.Admin;
using General.Mvc.Filters;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.Controllers.Admin
{
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    [ServiceFilter(typeof(ResponseLogTimeFilter))]
    public class FileController : Controller
    {
        public FileController()
        {

        }



        #region 文件管理

        /// <summary>
        /// 文件管理接口
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public IActionResult Getfile([FromServices] IWebHostEnvironment environment, string dir)
        {
            List<AppLocalFileModel> list = new List<AppLocalFileModel>();
            DirectoryInfo localdir = new DirectoryInfo(environment.WebRootPath + "/upload" + dir);

            DirectoryInfo[] dii = localdir.GetDirectories();
            foreach (var item in dii)
            {
                AppLocalFileModel temp = new AppLocalFileModel()
                {
                    hasSm = false,
                    isDir = true,
                    name = item.Name,
                    smUrl = "",
                    type = "dir",
                    url = ""
                };
                list.Add(temp);
            }
            FileInfo[] fil = localdir.GetFiles();
            foreach (var item in fil)
            {
                var isimg = false;
                var fileExt = item.Extension.ToLower();
                if (fileExt.Contains("jpg") || fileExt.Contains("png") || fileExt.Contains("jpeg") || fileExt.Contains("gif"))
                {
                    isimg = true;
                }

                AppLocalFileModel temp = new AppLocalFileModel()
                {
                    hasSm = isimg,
                    isDir = false,
                    name = item.Name,
                    smUrl = isimg ? "/images/getpath?name=" + (dir == "/" ? "/" : dir + "/") + item.Name : "",
                    type = "file",
                    url = "/upload" + (dir == "/" ? "/" : dir + "/") + item.Name
                };
                list.Add(temp);
            }

            return Content(JsonHelper.Serialize(new { code = 200, data = list, mes = "sucess" }));
        }

        /// <summary>
        /// 删除文件接口
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public IActionResult DeleteFile([FromServices] IWebHostEnvironment environment, [FromForm] string name)
        {
            AjaxResult result = new AjaxResult();
            name = name.Replace("/upload", "");
            string webRootPath = environment.WebRootPath;
            if (System.IO.File.Exists(webRootPath + "/upload" + name))
            {
                System.IO.File.Delete(webRootPath + "/upload" + name);
                result.code = AjaxResultStateEnum.Ok;
                result.message = "删除成功!";
            }
            else
            {
                result.code = AjaxResultStateEnum.Fail;
                result.message = "不存在此文件!";
            }
            return Content(JsonHelper.Serialize(result));
        }

        /// <summary>
        /// 创建文件夹接口
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> CreateFolder([FromServices] IWebHostEnvironment environment, [FromForm] string name, [FromForm] string dir = "/")
        {
            AjaxResult result = new AjaxResult();
            string webRootPath = environment.WebRootPath;
            dir = (dir == "/" ? "/" : dir + "/") + name;
            if (!Directory.Exists(webRootPath + "/upload" + dir))
            {
                Directory.CreateDirectory(webRootPath + "/upload" + dir);
                result.code = AjaxResultStateEnum.Ok;
                result.message = "创建成功";
            }
            else
            {
                result.code = AjaxResultStateEnum.Fail;
                result.message = "已存在同名文件夹!";
            }

            return Content(JsonHelper.Serialize(result));
        }

        #endregion

    }
}
