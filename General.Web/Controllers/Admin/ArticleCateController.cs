﻿using General.Entities.Models;
using General.Framework;
using General.Mvc.Filters;
using General.Services.Article;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.Controllers.Admin
{
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    [ServiceFilter(typeof(ResponseLogTimeFilter))]
    public class ArticleCateController : Controller
    {
        private readonly IArticleCateService articleCateService;

        public ArticleCateController(IArticleCateService articleCateService)
        {
            this.articleCateService = articleCateService;
        }


        /// <summary>
        /// 查询文章类别列表
        /// </summary>
        [HttpGet]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> GetArticleCateList(int page, int limit, string name)
        {
            var list = await articleCateService.GetList(page, limit, name);
            return Content(JsonHelper.Serialize(list));
        }


        /// <summary>
        /// 增加文章类别
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> AddArticleCateInfo([FromForm] tc_articlecate model)
        {
            await articleCateService.AddInfo(model);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "增加成功!" }));
        }

        /// <summary>
        /// 编辑文章类别
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> EditArticleCateInfo([FromForm] tc_articlecate model)
        {
            var res = await articleCateService.EditInfo(model);
            if (res > 0)
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "修改成功!" }));

            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Fail, message = "出现错误！" }));
        }

        /// <summary>
        /// 删除文章类别
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> DeleteArticleCate([FromForm] string ids)
        {
            var res = await articleCateService.DeleteInfo(ids);
            if (res > 0)
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message = "删除成功！" }));
            }
            else
            {
                return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Fail, message = "出现错误！" }));
            }
        }
    }
}
