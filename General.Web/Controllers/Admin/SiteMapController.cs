﻿using General.Core.Common.SEO;
using General.Core.Data;
using General.Entities.Models;
using General.Framework;
using General.Mvc.Filters;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.Controllers.Admin
{
    [Route("api/admin/[controller]/[action]")]
    [ApiController]
    [ServiceFilter(typeof(ResponseLogTimeFilter))]
    public class SiteMapController : Controller
    {
        private readonly IMemoryCache memoryCache;
        private readonly IRepository<tc_article> tc_ArticleRepository;
        private readonly IRepository<tc_articlecate> tc_ArticleCateRepository;
        private readonly IRepository<tc_diypage> tc_DiyPageRepository;

        public SiteMapController(IMemoryCache memoryCache,
            IRepository<tc_article> tc_articleRepository,
            IRepository<tc_articlecate> tc_articleCateRepository,
             IRepository<tc_diypage> tc_diyPageRepository)
        {
            this.memoryCache = memoryCache;
            tc_ArticleRepository = tc_articleRepository;
            tc_ArticleCateRepository = tc_articleCateRepository;
            tc_DiyPageRepository = tc_diyPageRepository;
        }

        /// <summary>
        /// 生成网站地图
        /// </summary>
        [HttpPost]
        [ServiceFilter(typeof(AdminCheckFilter))]
        public async Task<IActionResult> CreateMapFile([FromServices] IWebHostEnvironment environment,int urlCount=30000)
        {
            List<PageInfo> dataList = new List<PageInfo>();
            var indexUrl = memoryCache.Get("WebSite_Url").ToString();
            // 首页 文章分类 单页 文章
            dataList.Add(new PageInfo() { loc= indexUrl , lastmod = DateTime.Now });

            var cateList = await tc_ArticleCateRepository.Table.Where(x => x.status == 1).OrderBy(x => x.id).ToListAsync();
            foreach (var item in cateList)
            {
                dataList.Add(new PageInfo() {
                    lastmod = item.updateTime,
                    loc = (indexUrl + "/List/" + item.id.ToString() + ".html")
                });
            }
            


            var pageList = await tc_DiyPageRepository.Table.Where(x => x.status == 1).OrderBy(x => x.id).ToListAsync();
            foreach (var item in pageList)
            {
                dataList.Add(new PageInfo()
                {
                    lastmod = item.updateTime,
                    loc = (indexUrl + "/Page/" + item.route + ".html")
                });
            }


            var articleList = await tc_ArticleRepository.Table.Where(x => x.status == 1).OrderBy(x => x.id).ToListAsync();
            foreach (var item in articleList)
            {
                dataList.Add(new PageInfo()
                {
                    lastmod = item.updateTime,
                    loc = (indexUrl + "/Detail/" + item.id.ToString() + ".html")
                });
            }


            GenerateSiteMap siteMap = new GenerateSiteMap();
            siteMap.UrlList = dataList;
            var path = environment.WebRootPath;
            await siteMap.SaveSiteMap(path,urlCount);
            return Content(JsonHelper.Serialize(new AjaxResult() { code = AjaxResultStateEnum.Ok, message="生成网站地图成功！" }));
        }



    }
}
