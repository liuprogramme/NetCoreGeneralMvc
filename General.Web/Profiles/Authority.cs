﻿using AutoMapper;
using General.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Mvc.Profiles
{
    public class Authority: Profile
    {
        public Authority()
        {
            CreateMap<tc_authority, Authority>();
        }
    }
}
