﻿using System;
using AutoMapper;
using General.Entities.Models;
using General.Framework.Admin;

namespace General.Mvc.Profiles
{
    public class NoteProfile : Profile
    {
        public NoteProfile()
        {
            CreateMap<tc_note, AdminNoteModel>()
              .ForMember(dest => dest.time,
              opt => opt.MapFrom(src => src.intime.ToString("yyyy-MM-dd HH:mm"))).
              ForMember(dest => dest.content,
              opt => opt.MapFrom(src => src.message));
        }
    }
}
