﻿using AutoMapper;
using General.Entities.Models;
using General.Entities.ViewData.Department;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace General.Web.Profiles
{
    public class DepartmentProfile : Profile
    {
        public DepartmentProfile()
        {
            CreateMap<tc_department, DepartmentModel>()
             .ForMember(dest => dest.organizationId,
             opt => opt.MapFrom(src => src.id))
             .ForMember(dest => dest.parentId,
             opt => opt.MapFrom(src => src.parent_id))
             .ForMember(dest => dest.organizationName,
             opt => opt.MapFrom(src => src.name))
             .ForMember(dest => dest.sortNumber,
             opt => opt.MapFrom(src => src.seq))
             .ForMember(dest => dest.createTime,
             opt => opt.MapFrom(src => src.createTime.ToString("yyyy-MM-dd HH:mm"))).
             ForMember(dest => dest.updateTime,
             opt => opt.MapFrom(src => src.updateTime.ToString("yyyy-MM-dd HH:mm")));
        }
    }
}
