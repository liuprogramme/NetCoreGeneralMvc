﻿using System;
using System.Diagnostics;
using System.Threading.Tasks;
using General.Services.SystemLogs;
using Microsoft.Extensions.Logging;
using Quartz;

namespace General.Mvc.Jobs
{
    public class ClearSystemLogsJob : IJob
    {
        private readonly ISystemLogsService systemLogsService;
        private readonly ILogger<ClearSystemLogsJob> logger;

        public ClearSystemLogsJob(ISystemLogsService systemLogsService,
            ILogger<ClearSystemLogsJob> logger)
        {
            this.systemLogsService = systemLogsService;
            this.logger = logger;
        }

        public Task Execute(IJobExecutionContext context)
        {
            return Task.Run(async () =>
            {
                Stopwatch stopwatch = Stopwatch.StartNew();
                var resCount = await systemLogsService.DeleteAll();
                var timeElapsed = stopwatch.Elapsed;
                logger.LogInformation(new Exception("清空数:"+ resCount),"定时清空系统日志 耗时:"+timeElapsed.Milliseconds.ToString() + "ms");
            });
        }
    }
}
