﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using General.Entities.Data;
using General.Entities.Models;
using General.Entities.ViewData.Role;

namespace General.Services.Role
{
    public interface IRoleService
    {

        /// <summary>
        /// 获取所有数据
        /// </summary>
        /// <returns></returns>
        Task<List<RoleModel>> GetAllList();

        /// <summary>
        /// 获取所有角色数据写入内存
        /// </summary>
        /// <returns></returns>
        Task GetDataWriteMemory();


        /// <summary>
        /// 根据id列表获取role列表
        /// </summary>
        /// <param name="uids"></param>
        /// <returns></returns>
        Task<List<tc_role>> GetListByIds(string uids);


        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="rolename"></param>
        /// <param name="rolecode"></param>
        /// <param name="remark"></param>
        /// <returns></returns>
        Task<AdminResult> GetList(int page, int limit, string rolename, string rolecode, string remark);


        /// <summary>
        /// 增加信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task AddInfo(tc_role model);

        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<int> DeleteInfo(string ids);

        /// <summary>
        /// 修改信息
        /// </summary>
        /// <param name="id"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        Task EditInfo(int id, tc_role role);

        /// <summary>
        /// 根据id获取权限功能id列表
        /// </summary>
        /// <param name="roleid"></param>
        /// <returns></returns>
        Task<List<int>> GetPermissionById(int roleid);

        /// <summary>
        /// 获取角色权限配置
        /// </summary>
        /// <returns></returns>
        Task<List<PermissionmModel>> GetPermissionmData(int roleid);

        /// <summary>
        /// 修改角色权限信息
        /// </summary>
        /// <returns></returns>
        Task EditPermissionInfo(int id, string data);

    }
}
