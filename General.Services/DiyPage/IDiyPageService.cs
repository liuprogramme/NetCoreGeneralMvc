﻿using General.Entities.Data;
using General.Entities.Models;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Services.DiyPage
{
    public interface IDiyPageService
    {

        /// <summary>
        /// 获取列表信息    
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        Task<AdminResult> GetList(int page, int limit, string name);


        /// <summary>
        /// 获取所有正常状态页面列表（路由）  
        /// </summary>
        /// <returns></returns>
        Task<List<tc_diypage>> GetNormalStausList();

        /// <summary>
        /// 判断是否有重名路由
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Task<bool> IsHasRoute(string name, int? id = null);

        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task AddInfo(tc_diypage model);

        /// <summary>
        /// 后台编辑数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<int> EditInfo(tc_diypage model);


        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<int> DeleteInfo(string ids);


        /// <summary>
        /// 获取页面html
        /// </summary>
        /// <returns></returns>
        public Task<IActionResult> GetPageHtml(string name);
    }
}
