﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Services.SystemStartUp
{
    public interface ISystemStartUpService
    {


        /// <summary>
        /// 应用启动获取基础配置
        /// </summary>
        /// <returns></returns>
        public Task InitializationConfiguration();


        /// <summary>
        /// 获取网站配置
        /// </summary>
        /// <returns></returns>
        public Task GetWebSiteConfig();
    }
}
