﻿using General.Core.Common.OAuth.WeChat;
using General.Core.Config;
using General.Core.Data;
using General.Entities.Models;
using General.Services.Authority;
using General.Services.Role;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Caching.Memory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Services.SystemStartUp
{
    public class SystemStartUpService : ISystemStartUpService
    {
        private readonly IMemoryCache memoryCache;
        private readonly IRepository<tc_sysconfig> tc_SysconfigRepository;
        private readonly IRoleService roleService;
        private readonly IAuthorityService authorityService;

        public SystemStartUpService(IMemoryCache memoryCache,
            IRepository<tc_sysconfig> tc_sysconfigRepository,
            IRoleService roleService,
            IAuthorityService authorityService)
        {
            this.memoryCache = memoryCache;
            tc_SysconfigRepository = tc_sysconfigRepository;
            this.roleService = roleService;
            this.authorityService = authorityService;
        }


        /// <summary>
        /// 应用启动获取基础配置
        /// </summary>
        /// <returns></returns>
        public async Task InitializationConfiguration()
        {
            await GetWebSiteConfig();

            //获取角色数据
            
            await roleService.GetDataWriteMemory();
            await authorityService.GetDataWriteMemory();
        }


        /// <summary>
        /// 获取网站配置
        /// </summary>
        /// <returns></returns>
        public async Task GetWebSiteConfig()
        {
            var siteConfigResult = await tc_SysconfigRepository.GetAllAsync(x=>x.code.StartsWith(GobalConfig.SYSTEM_CONFIG_STRING) || x.code.StartsWith(GobalConfig.SYSTEM_MAIL_STRING) || x.code.StartsWith(GobalConfig.SYSTEM_SEO_STRING)).ToListAsync();
            foreach (var item in siteConfigResult)
            {
                memoryCache.Set(item.code,item.value);


                //获取缓存信息
                if (item.code.StartsWith(GobalConfig.SYSTEM_CONFIG_STRING))
                {
                    switch (item.code)
                    {
                        case nameof(WebSiteTemplateConfig.WebSite_OpenTemplateCache):
                            WebSiteTemplateConfig.WebSite_OpenTemplateCache = item.value == "1" ? true : false;
                            break;
                        case nameof(WebSiteTemplateConfig.WebSite_OpenDataCache):
                            WebSiteTemplateConfig.WebSite_OpenDataCache = item.value == "1" ? true : false;
                            break;
                        case nameof(WebSiteTemplateConfig.WebSite_OpenMainCache):
                            WebSiteTemplateConfig.WebSite_OpenMainCache = item.value == "1" ? true : false;
                            break;
                        case nameof(WebSiteConfig.WebSite_IsOpen):
                            WebSiteConfig.WebSite_IsOpen = item.value == "1" ? true : false;
                            break;
                        case "WebSite_OpenWechatLogin":
                            WeChatDefaults.OpenWechatLogin = item.value == "1" ? true : false;
                            break;
                        case "WebSite_WechatAppId":
                            WeChatDefaults.AppId = item.value;
                            break;
                        case "WebSite_WechatAppSecret":
                            WeChatDefaults.AppSecret = item.value;
                            break;
                    }
                }
                
                //获取邮箱配置
                if (item.code.StartsWith(GobalConfig.SYSTEM_MAIL_STRING))
                {
                    switch (item.code)
                    {
                        case nameof(MailConfig.Mail_SendMail):
                            MailConfig.Mail_SendMail = item.value;
                            break;
                        case nameof(MailConfig.Mail_Address):
                            MailConfig.Mail_Address = item.value;
                            break;
                        case nameof(MailConfig.Mail_Port):
                            MailConfig.Mail_Port = item.value;
                            break;
                        case nameof(MailConfig.Mail_UserName):
                            MailConfig.Mail_UserName = item.value;
                            break;
                        case nameof(MailConfig.Mail_PassWord):
                            MailConfig.Mail_PassWord = item.value;
                            break;
                    }
                }
            }
        }


    }
}
