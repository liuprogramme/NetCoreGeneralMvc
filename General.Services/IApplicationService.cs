﻿using Panda.DynamicWebApi;
using Panda.DynamicWebApi.Attributes;
using System;
using System.Collections.Generic;
using System.Text;

namespace General.Services
{
    /// <summary>
    /// 应用逻辑接口
    /// </summary>
    [DynamicWebApi]
    public interface IApplicationService: IDynamicWebApi
    {
    }
}
