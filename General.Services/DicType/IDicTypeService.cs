using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using General.Entities.Data;
using General.Entities.Models;

namespace General.Services.DicType
{
    public interface IDicTypeService
    {

        /// <summary>
        /// 获取列表信息    
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        Task<AdminResult> GetList(int page, int limit, string name);
        
        /// <summary>
        /// 增加信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task AddInfo(tc_dictype model);

        /// <summary>
        /// 修改信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task EditInfo(tc_dictype data);

        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<int> DeleteInfo(string ids);
    }
}