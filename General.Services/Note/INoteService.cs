﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using General.Entities.Models;

namespace General.Services.Note
{
    public interface INoteService
    {
        /// <summary>
        /// 获取所有便签
        /// </summary>
        /// <returns></returns>
        Task<List<tc_note>> GetAllList();

        /// <summary>
        /// 更新便签内容
        /// </summary>
        /// <param name="id"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        Task<int> UpdateInfo(int id,string message);

        /// <summary>
        /// 删除便签
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<int> DeleteInfo(int id);


        /// <summary>
        /// 增加便签
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<int> AddInfo(string message,int uid);

    }
}
