﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using General.Core.Data;
using General.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System.Linq;

namespace General.Services.Note
{
    public class NoteService : INoteService
    {
        private readonly IRepository<tc_note> tc_NoteRepository;

        public NoteService(IRepository<tc_note> tc_noteRepository)
        {
            tc_NoteRepository = tc_noteRepository;
        }

        /// <summary>
        /// 删除便签
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<int> DeleteInfo(int id)
        {
            var model = await tc_NoteRepository.getByIdAsync(id);
            var res = await tc_NoteRepository.DeleteAsync(model);
            return res;
        }

        /// <summary>
        /// 获取所有便签
        /// </summary>
        /// <returns></returns>
        public async Task<List<tc_note>> GetAllList()
        {
            return await tc_NoteRepository.GetAllAsync().OrderBy(x=>x.id).ToListAsync();
        }

        /// <summary>
        /// 更新便签
        /// </summary>
        /// <param name="id"></param>
        /// <param name="message"></param>
        /// <returns></returns>
        public async Task<int> UpdateInfo(int id, string message)
        {
            var model = await tc_NoteRepository.getByIdAsync(id);
            model.message = message;
            return await tc_NoteRepository.UpdateAsync(model);
        }


        /// <summary>
        /// 增加便签
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public async Task<int> AddInfo(string message,int uid)
        {
            var model = new tc_note()
            {
                 intime = DateTime.Now, message = message, uid = uid
            };
            return await tc_NoteRepository.insertAsync(model);
        }
    }
}
