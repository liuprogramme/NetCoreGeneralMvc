﻿using General.Core.Config;
using General.Entities.Data;
using General.Entities.ViewData.Template;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Services.Template
{
    public interface ITemplateService
    {

        /// <summary>
        /// 获取模板信息列表
        /// </summary>
        /// <returns></returns>
        AdminResult GetTemplatePages();
        



        /// <summary>
        /// 获取模板文件夹列表
        /// </summary>
        /// <returns></returns>
        List<TemplateModel> GetTemplateList();


        /// <summary>
        /// 设置模板文件
        /// </summary>
        /// <param name="name"></param>
        /// <returns></returns>
        Task<int> SetTemplateName(string name);



        /// <summary>
        /// 清除模板缓存
        /// </summary>
        /// <returns></returns>
        void ClearTemplateCache();


        /// <summary>
        /// 清除数据缓存
        /// </summary>
        /// <returns></returns>
        Task ClearDataCache();

        /// <summary>
        /// 清除主页缓存
        /// </summary>
        /// <returns></returns>
        void ClearMainCache();


        /// <summary>
        /// 加载所有模板页面
        /// </summary>
        /// <returns></returns>
        Task GetTemplateHtmlPages();


        /// <summary>
        /// 获取模板渲染后html内容
        /// </summary>
        /// <param name="templateName"></param>
        /// <returns></returns>
        Task<string> GetPageHtmlContent(string templateName,string parm="");

    }
}
