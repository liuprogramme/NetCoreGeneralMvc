﻿using System;
using System.Linq;
using System.Threading.Tasks;
using General.Core.Data;
using General.Entities.Data;
using General.Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace General.Services.Dictionary
{
    public class DictionaryService: IDictionaryService
    {
        private readonly IRepository<tc_dictionary> tc_DictionaryRepository;

        public DictionaryService(IRepository<tc_dictionary> tc_dictionaryRepository)
        {
            tc_DictionaryRepository = tc_dictionaryRepository;
        }
       
        /// <summary>
        /// 增加信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task<int> AddInfo(tc_dictionary model)
        {
            var res = await tc_DictionaryRepository.GetAsync(x=>x.dicname == model.dicname);
            if (res==null)
            {
                model.createtime = DateTime.Now;
                return await tc_DictionaryRepository.insertAsync(model);
            }
            else
            {
                return 0;
            }
        }


        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<int> DeleteInfo(string ids)
        {
            var arr = ids.Split(",",StringSplitOptions.RemoveEmptyEntries);
            var arrints = arr.Select<string, int>(x => Convert.ToInt32(x));
            var list = await tc_DictionaryRepository.GetAllAsync(x=>arrints.Contains(x.id)).ToListAsync();
            return await tc_DictionaryRepository.DeleteListAsync(list);
        }

        /// <summary>
        /// 编辑信息
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task EditInfo(tc_dictionary data)
        {
            var model = await tc_DictionaryRepository.getByIdAsync(data.id);
            model.dicname = data.dicname;
            model.dicode = data.dicode;
            model.remark = data.remark;
            model.sort = data.sort;
            await tc_DictionaryRepository.UpdateAsync(model);
        }


        /// <summary>
        /// 查询列表信息
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="typeid"></param>
        /// <param name="dicname"></param>
        /// <param name="dicode"></param>
        /// <returns></returns>
        public async Task<AdminResult> GetList(int page, int limit, int typeid, string dicname, string dicode)
        {
            AdminResult result = new AdminResult();
            var query = from d in tc_DictionaryRepository.Table
                        where d.typeid == typeid
                        orderby d.sort //descending
                        select new tc_dictionary()
                        {
                            id = d.id,
                            createtime = d.createtime,
                            dicname = d.dicname, sort = d.sort, dicode=d.dicode, remark = d.remark, typeid = d.typeid
                        };
            if (!string.IsNullOrWhiteSpace(dicname))
            {
                query = query.Where(x => x.dicname.Contains(dicname));
            }
            if (!string.IsNullOrWhiteSpace(dicode))
            {
                query = query.Where(x => x.dicode.Contains(dicode));
            }
            result.count = await query.CountAsync();
            result.data = await query.Skip((page - 1) * limit).Take(limit).ToListAsync();
            result.code = 0;
            return result;
        }
    }
}
