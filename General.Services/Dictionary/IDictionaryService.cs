﻿using System;
using System.Threading.Tasks;
using General.Entities.Data;
using General.Entities.Models;

namespace General.Services.Dictionary
{
    public interface IDictionaryService
    {
        /// <summary>
        /// 获取列表信息    
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        Task<AdminResult> GetList(int page, int limit, int typeid,string dicname,string dicode);

        /// <summary>
        /// 增加信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<int> AddInfo(tc_dictionary model);

        /// <summary>
        /// 修改信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task EditInfo(tc_dictionary data);

        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<int> DeleteInfo(string ids);

    }
}
