﻿using General.Entities.Data;
using General.Entities.Models;
using General.Entities.ViewData.Department;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Services.Department
{
    public interface IDepartmentService
    {

        /// <summary>
        /// 获取列表信息    
        /// </summary>
        /// <returns></returns>
        Task<AdminResult> GetList();


        /// <summary>
        /// 根据id查询
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<tc_department> GetDataByID(int id);

        /// <summary>
        /// 添加机构数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task AddInfo(DepartmentModel model,int uid);

        /// <summary>
        /// 编辑机构数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<int> EditInfo(DepartmentModel model, int uid);

        /// <summary>
        /// 删除机构数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<int> DeleteInfo(int id);


        /// <summary>
        /// 是否账号已存在此部门
        /// </summary>
        /// <param name="did"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        Task<bool> IsExistAccountDepartment(int did, string uid);


        /// <summary>
        /// 将用户添加到机构
        /// </summary>
        /// <param name="did"></param>
        /// <param name="uid"></param>
        /// <returns></returns>
        Task AddAccountDepartment(int did,string uid,int cuserId);


        /// <summary>
        /// 批量删除部门所在的用户
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<int> DeleteAccountDepartment(string ids);
    }
}
