﻿using General.Entities.Data;
using General.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Services.Article
{
    public interface IArticleCateService
    {
        /// <summary>
        /// 获取列表信息    
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        Task<AdminResult> GetList(int page, int limit, string name);



        /// <summary>
        /// 根据ID获取信息
        /// </summary>
        /// <returns></returns>
        Task<tc_articlecate> GetInfoById(int id);


        /// <summary>
        /// 获取所有列表 接口调用
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        Task<List<tc_articlecate>> GetListBySort();


        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task AddInfo(tc_articlecate model);


        /// <summary>
        /// 后台编辑数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<int> EditInfo(tc_articlecate model);


        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<int> DeleteInfo(string ids);

    }
}
