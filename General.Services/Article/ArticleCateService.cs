﻿using General.Core.Data;
using General.Entities.Data;
using General.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace General.Services.Article
{
    public class ArticleCateService: IArticleCateService
    {
        private readonly IRepository<tc_articlecate> tc_ArticleCateRepository;
        private readonly IRepository<tc_article> tc_ArticleRepository;

        public ArticleCateService(IRepository<tc_articlecate> tc_articleCateRepository,
            IRepository<tc_article> tc_articleRepository)
        {
            tc_ArticleCateRepository = tc_articleCateRepository;
            tc_ArticleRepository = tc_articleRepository;
        }

        /// <summary>
        /// 获取列表信息
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<AdminResult> GetList(int page, int limit, string name)
        {
            AdminResult result = new AdminResult();
            var query = tc_ArticleCateRepository.Table.Where(x => !string.IsNullOrWhiteSpace(name) ? x.name.Contains(name) : true).OrderByDescending(x => x.id);
            result.count = await query.CountAsync();
            result.data = await query.Skip((page - 1) * limit).Take(limit).ToListAsync();
            result.code = 0;
            return result;
        }


        /// <summary>
        /// 根据ID获取信息
        /// </summary>
        /// <returns></returns>
        public async Task<tc_articlecate> GetInfoById(int id)
        {
            return await tc_ArticleCateRepository.getByIdAsync(id);
        }


        /// <summary>
        /// 获取所有列表 接口调用
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public async Task<List<tc_articlecate>> GetListBySort()
        {
            var result = await tc_ArticleCateRepository.GetAllAsync(x=>true).OrderByDescending(x=>x.sort).ToListAsync();
            return result;
        }


        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task AddInfo(tc_articlecate model)
        {
            model.createTime = DateTime.Now;
            model.updateTime = DateTime.Now;
            model.count = 0;
            await tc_ArticleCateRepository.insertAsync(model);
        }



        /// <summary>
        /// 后台编辑数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<int> EditInfo(tc_articlecate model)
        {
            model.updateTime = DateTime.Now;
            Expression<Func<tc_articlecate, object>>[] updatedProperties = {
                    p => p.updateTime,p=>p.description,p=>p.image,p=>p.keywords,p=>p.name,p=>p.sort,p=>p.status
                };
            return await tc_ArticleCateRepository.UpdateByFieldsAsync(model, updatedProperties);
        }


        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<int> DeleteInfo(string ids)
        {
            var arr = ids.Split(",", StringSplitOptions.RemoveEmptyEntries);
            List<tc_articlecate> list = new List<tc_articlecate>();
            foreach (var item in arr)
            {
                list.Add(new tc_articlecate() { id = Convert.ToInt32(item) });
                //删除文章
                await tc_ArticleRepository.ExecuteSqlAsync("delete from tc_article where cateId="+ item);
            }
            
            return await tc_ArticleCateRepository.DeleteListByFieldsAsync(list);
        }

    }
}
