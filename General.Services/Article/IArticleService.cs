﻿using General.Entities.Data;
using General.Entities.Models;
using General.Entities.ViewData.Article;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Services.Article
{
    public interface IArticleService
    {
        /// <summary>
        /// 获取列表信息    
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        Task<AdminResult> GetList(DateTime? startTime, DateTime? endTime,int page, int limit, string title, int cateId = -1, int status=-1,int ontop=-1,int iselite=-1);



        /// <summary>
        /// 获取最新文章
        /// </summary>
        /// <returns></returns>
        Task<List<SimpleArticleInfo>> GetNewList(int limit= 10,int page=1,int? cate=null, string? search=null);

        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task AddInfo(tc_article model);

        /// <summary>
        /// 根据id获取文章内容
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<string> GetArticleContent(int id);


        /// <summary>
        /// 根据id获取文章详细信息 渲染前端
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<ArticleInfo> GetArticleInfoById(int id);

        /// <summary>
        /// 后台编辑数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<int> EditInfo(tc_article model);

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<int> DeleteInfo(string ids);


        /// <summary>
        /// 增加文章点击量
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task AddClickCount(int id,int hits);



        /// <summary>
        /// 根据时间获取文章数量
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        Task<int> GetCountByDate(DateTime? date);

    }
}
