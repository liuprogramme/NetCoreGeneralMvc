﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using General.Entities.Data;
using General.Entities.Models;

namespace General.Services.SystemLogs
{
    public interface ISystemLogsService
    {
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="page">页数</param>
        /// <param name="limit">显示数量</param>
        /// <param name="message">日志内容</param>
        /// <param name="exception">异常信息</param>
        /// <param name="level">等级</param>
        /// <returns></returns>
        Task<AdminResult> GetList(int page,int limit,string message,string exception,string level);

        /// <summary>
        /// 删除所有数据
        /// </summary>
        /// <returns></returns>
        Task<int> DeleteAll();


        /// <summary>
        /// 删除指定id
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<int> DeleteByIds(string ids);

    }
}
