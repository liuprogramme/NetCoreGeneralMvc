﻿using General.Entities.Data;
using General.Entities.Models;
using General.Entities.ViewData.Authority;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Services.Authority
{
    public interface IAuthorityService
    {
        /// <summary>
        /// 获取列表信息    
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        Task<AdminResult> GetList(string authorityName, string menuUrl, string authority);

        /// <summary>
        /// 获取全部可用状态数据
        /// </summary>
        /// <returns></returns>
        Task<List<tc_authority>> GetIsUseData();

        /// <summary>
        /// 获取所有角色数据写入内存
        /// </summary>
        /// <returns></returns>
        Task GetDataWriteMemory();

        /// <summary>
        /// 增加信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<int> AddInfo(AuthorityModel model);

        /// <summary>
        /// 修改信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task<int> EditInfo(AuthorityModel model);

        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<int> DeleteInfo(string ids);

        /// <summary>
        /// 判断上级是否为菜单
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> IsMenuById(int id);


        /// <summary>
        /// 判断标识是否已存在
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> IsHasAuthority(string name, int? id = null);

    }
}
