﻿using General.Core.Data;
using General.Entities.Data;
using General.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace General.Services.Slider
{
    public class SliderService: ISliderService
    {
        private readonly IRepository<tc_slider> tc_SliderRepository;

        public SliderService(IRepository<tc_slider> tc_sliderRepository)
        {
            tc_SliderRepository = tc_sliderRepository;
        }

        /// <summary>
        /// 获取列表信息    
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public async Task<AdminResult> GetList(int page, int limit, string name)
        {
            AdminResult result = new AdminResult();
            var query = tc_SliderRepository.Table.Where(x => !string.IsNullOrWhiteSpace(name) ? x.title.Contains(name) : true).OrderByDescending(x=>x.id);
            result.count = await query.CountAsync();
            result.data = await query.Skip((page - 1) * limit).Take(limit).ToListAsync();
            result.code = 0;
            return result;
        }

        /// <summary>
        /// 获取正常状态所有列表信息    
        /// </summary>
        /// <returns></returns>
        public async Task<List<tc_slider>> GetIsUseList()
        {
            return await tc_SliderRepository.GetAllAsync(x => x.status == 1).OrderBy(x => x.sort).ToListAsync();
        }


        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task AddInfo(tc_slider model)
        {
            model.createTime = DateTime.Now;
            model.updateTime = DateTime.Now;
            await tc_SliderRepository.insertAsync(model);
        }


        /// <summary>
        /// 后台编辑数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<int> EditInfo(tc_slider model)
        {
            model.updateTime = DateTime.Now;
            Expression<Func<tc_slider, object>>[] updatedProperties = {
                    p => p.updateTime,p=>p.img,p=>p.status,p=>p.title,p=>p.url,p=>p.sort
                };
            return await tc_SliderRepository.UpdateByFieldsAsync(model, updatedProperties);
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<int> DeleteInfo(string ids)
        {
            var arr = ids.Split(",", StringSplitOptions.RemoveEmptyEntries);
            List<tc_slider> list = new List<tc_slider>();
            foreach (var item in arr)
            {
                list.Add(new tc_slider() { id = Convert.ToInt32(item) });
            }

            return await tc_SliderRepository.DeleteListByFieldsAsync(list);
        }


    }
}
