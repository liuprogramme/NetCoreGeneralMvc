using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using General.Entities.Data;
using General.Entities.Models;

namespace General.Services.Navigation
{
    public interface INavigationService
    {
        /// <summary>
        /// 获取列表信息    
        /// </summary>
        Task<AdminResult> GetList(string name, string menuUrl);

        /// <summary>
        /// 获取正常状态所有列表信息    
        /// </summary>
        Task<List<tc_navigation>> GetIsUseList();

        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task AddInfo(tc_navigation model);

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        Task<int> DeleteInfo(string ids);

        /// <summary>
        /// 后台编辑数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<int> EditInfo(tc_navigation data);

    }
}
