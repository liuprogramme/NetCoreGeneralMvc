using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;
using General.Core.Data;
using General.Entities.Data;
using General.Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace General.Services.Navigation
{
    public class NavigationService : INavigationService
    {
        private readonly IRepository<tc_navigation> tc_navigationRepository;

        public NavigationService(IRepository<tc_navigation> tc_navigationRepository)
        {
            this.tc_navigationRepository = tc_navigationRepository;
        }
        /// <summary>
        /// 获取列表信息    
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        public async Task<AdminResult> GetList(string name, string menuUrl)
        {
            AdminResult result = new AdminResult();
            var query = tc_navigationRepository.Table.Where(x => !string.IsNullOrWhiteSpace(name) ? x.name.Contains(name) : true && !string.IsNullOrWhiteSpace(menuUrl) ? x.menuUrl.Contains(menuUrl) : true).OrderBy(x => x.orderNumber);
            result.count = await query.CountAsync();
            result.data = await query.ToListAsync();
            result.code = 0;
            return result;
        }

        /// <summary>
        /// 获取正常状态所有列表信息    
        /// </summary>
        /// <returns></returns>
        public async Task<List<tc_navigation>> GetIsUseList()
        {
            return await tc_navigationRepository.GetAllAsync(x=>x.isUse == 1).OrderBy(x => x.orderNumber).ToListAsync();
        }


        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task AddInfo(tc_navigation model)
        {
            model.parentId = string.IsNullOrEmpty(model.parentId.ToString()) || model.parentId == -1 ? -1 : model.parentId;
            model.createTime = DateTime.Now;
            model.updateTime = DateTime.Now;
            await tc_navigationRepository.insertAsync(model);
        }

        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<int> DeleteInfo(string ids)
        {
            var arr = ids.Split(",", StringSplitOptions.RemoveEmptyEntries);
            List<tc_navigation> list = new List<tc_navigation>();
            foreach (var item in arr)
            {
                list.Add(new tc_navigation() { id = Convert.ToInt32(item) });
            }

            return await tc_navigationRepository.DeleteListByFieldsAsync(list);
        }

        /// <summary>
        /// 后台编辑数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<int> EditInfo(tc_navigation model)
        {
            model.parentId = string.IsNullOrEmpty(model.parentId.ToString()) || model.parentId == -1 ? -1 : model.parentId;
            model.updateTime = DateTime.Now;
            Expression<Func<tc_navigation, object>>[] updatedProperties = {
                    p => p.updateTime,p=>p.parentId,
                    p=>p.name,p=>p.menuUrl,p=>p.orderNumber,p=>p.isUse
                };
            return await tc_navigationRepository.UpdateByFieldsAsync(model, updatedProperties);
        }


    }
}
