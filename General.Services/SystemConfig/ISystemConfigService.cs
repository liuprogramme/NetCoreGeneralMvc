﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using General.Entities.Models;
using General.Entities.ViewData.SystemConfig;

namespace General.Services.SystemConfig
{
    public interface ISystemConfigService
    {
        /// <summary>
        /// 获取类别列表    
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        Task<List<tc_sysconftype>> GetTypeList();

        /// <summary>
        /// 根据标识获取配置信息
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        Task<tc_sysconfig> GetConfigInfoByCode(string code);

        /// <summary>
        /// 获取所有配置信息
        /// </summary>
        /// <param name="code"></param>
        /// <returns></returns>
        Task<List<tc_sysconfig>> GetAllConfigInfo();

        /// <summary>
        /// 添加配置信息
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        Task<int> AddConfigData(tc_sysconfig data);

        /// <summary>
        /// 修改配置信息
        /// </summary>
        /// <param name="tid"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        Task EditConfigData(int tid,List<SystemConfigModel> data);
    }
}
