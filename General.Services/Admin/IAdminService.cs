﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using General.Entities.Data;
using General.Entities.Models;

namespace General.Services.Admin
{
    public interface IAdminService
    {
        /// <summary>
        /// 根据用户名获取用户信息
        /// </summary>
        /// <returns></returns>
        Task<tc_admin> GetInfoByUserName(string name);

        /// <summary>
        /// 根据编号获取用户信息
        /// </summary>
        /// <returns></returns>
        Task<tc_admin> GetInfoById(int id);

        /// <summary>
        /// 更新登录时间信息
        /// </summary>
        /// <param name="uid"></param>
        /// <returns></returns>
        Task UpdateLoginStatusInfo(int uid,string ip);

        /// <summary>
        /// 修改头像
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="headimg"></param>
        /// <returns></returns>
        Task UpdateHeadImg(int uid,string headimg);

        /// <summary>
        /// 修改用户名称
        /// </summary>
        /// <returns></returns>
        Task UpdateUsername(tc_admin model, string username);

        /// <summary>
        /// 修改用户密码
        /// </summary>
        /// <param name="uid"></param>
        /// <param name="pwd"></param>
        /// <returns></returns>
        Task UpdateUserPwd(int uid ,string pwd);


        /// <summary>
        /// 获取用户列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="username"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        Task<AdminResult> GetList(int page,int limit,string username,int status=-1,int organizationId=-1);

        /// <summary>
        /// 增加用户
        /// </summary>
        /// <param name="username"></param>
        /// <param name="status"></param>
        /// <param name="role"></param>
        /// <returns></returns>
        Task<int> AddInfo(string username,int status,string role);


        /// <summary>
        /// 编辑用户
        /// </summary>
        /// <returns></returns>
        Task EditInfo(tc_admin data);

        /// <summary>
        /// 删除信息
        /// </summary>
        /// <returns></returns>
        Task<int> DeleteInfo(string ids);

        /// <summary>
        /// 修改用户状态
        /// </summary>
        /// <returns></returns>
        Task<int> EditStatus(int id,int status);


        /// <summary>
        /// 根据时间获取会员数量
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        Task<int> GetCountByDate(DateTime? date);


        /// <summary>
        /// 根据openid获取用户的id
        /// </summary>
        /// <param name="type"></param>
        /// <param name="openid"></param>
        /// <returns></returns>
        Task<int> GetIdByOpenId(string type,string openid);

        /// <summary>
        /// 绑定第三方登录Openid
        /// </summary>
        /// <returns></returns>
        Task BindOpenId(int uid,string type, string openid);

        /// <summary>
        /// 判断是否已经绑定过openid
        /// </summary>
        /// <param name="openid"></param>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<bool> IsHasOpenid(string type,string openid, int? uid = null);
    }
}
