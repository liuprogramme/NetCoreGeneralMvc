﻿using General.Core.Data;
using General.Entities.Data;
using General.Entities.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace General.Services.Link
{
    public class LinkService:ILinkService
    {
        private readonly IRepository<tc_link> tc_LinkRepository;

        public LinkService(IRepository<tc_link> tc_linkRepository)
        {
            tc_LinkRepository = tc_linkRepository;
        }

        /// <summary>
        /// 获取列表信息
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="name"></param>
        /// <returns></returns>
        public async Task<AdminResult> GetList(int page, int limit, string name)
        {
            AdminResult result = new AdminResult();
            var query = tc_LinkRepository.Table.Where(x => !string.IsNullOrWhiteSpace(name) ? x.name.Contains(name) : true).OrderByDescending(x => x.id);
            result.count = await query.CountAsync();
            result.data = await query.Skip((page - 1) * limit).Take(limit).ToListAsync();
            result.code = 0;
            return result;
        }

        /// <summary>
        /// 获取正常状态所有列表信息    
        /// </summary>
        /// <returns></returns>
        public async Task<List<tc_link>> GetIsUseList()
        {
            return await tc_LinkRepository.GetAllAsync(x => x.status == 1).OrderBy(x => x.sort).ToListAsync();
        }

        /// <summary>
        /// 添加数据
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task AddInfo(tc_link model)
        {
            model.createTime = DateTime.Now;
            model.updateTime = DateTime.Now;
            await tc_LinkRepository.insertAsync(model);
        }

        /// <summary>
        /// 后台编辑数据
        /// </summary>
        /// <param name="data"></param>
        /// <returns></returns>
        public async Task<int> EditInfo(tc_link model)
        {
            model.updateTime = DateTime.Now;
            Expression<Func<tc_link, object>>[] updatedProperties = {
                    p => p.updateTime,p=>p.nofollow,p=>p.url,p=>p.name,p=>p.sort,p=>p.status
                };
            return await tc_LinkRepository.UpdateByFieldsAsync(model, updatedProperties);
        }


        /// <summary>
        /// 批量删除
        /// </summary>
        /// <param name="ids"></param>
        /// <returns></returns>
        public async Task<int> DeleteInfo(string ids)
        {
            var arr = ids.Split(",", StringSplitOptions.RemoveEmptyEntries);
            List<tc_link> list = new List<tc_link>();
            foreach (var item in arr)
            {
                list.Add(new tc_link() { id = Convert.ToInt32(item) });
            }

            return await tc_LinkRepository.DeleteListByFieldsAsync(list);
        }

    }
}
