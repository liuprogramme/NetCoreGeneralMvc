﻿using System;
using System.Threading.Tasks;
using General.Entities.Data;
using General.Entities.Models;

namespace General.Services.Region
{
    public interface IRegionService
    {
        /// <summary>
        /// 获取列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <returns></returns>
        Task<AdminResult> GetList(int page, int limit, string name, int level);

        /// <summary>
        /// 增加信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        Task AddInfo(tc_region model);

        /// <summary>
        /// 删除信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task<int> DeleteInfo(string ids);

        /// <summary>
        /// 修改信息
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        Task EditInfo(tc_region data);
    }
}
