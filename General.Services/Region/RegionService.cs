﻿using System;
using System.Linq;
using System.Threading.Tasks;
using General.Core.Data;
using General.Entities.Data;
using General.Entities.Models;
using Microsoft.EntityFrameworkCore;

namespace General.Services.Region
{
    public class RegionService:IRegionService
    {
        private readonly IRepository<tc_region> tc_RegionRepository;

        public RegionService(IRepository<tc_region> tc_regionRepository)
        {
            tc_RegionRepository = tc_regionRepository;
        }

        /// <summary>
        /// 增加信息
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public async Task AddInfo(tc_region model)
        {
            await tc_RegionRepository.insertAsync(model);
        }

        public async Task<int> DeleteInfo(string ids)
        {
            var arr = ids.Split(",", StringSplitOptions.RemoveEmptyEntries);
            var arrints = arr.Select<string, int>(x => Convert.ToInt32(x));
            var res = 0;
            foreach (var item in arrints)
            {
                res+=await tc_RegionRepository.ExecuteSqlAsync("delete from tc_region where id="+item);
            }

            return res;
        }


        /// <summary>
        /// 编辑信息
        /// </summary>
        /// <returns></returns>
        public async Task EditInfo(tc_region data)
        {
            var model = await tc_RegionRepository.getByIdAsync(data.id);
            model.citycode = data.citycode;
            model.Lat = data.Lat;
            model.level = data.level;
            model.Lng = data.Lng;
            model.mername = data.mername;
            model.name = data.name;
            model.pid = data.pid;
            model.pinyin = data.pinyin;
            model.sname = data.sname;
            model.yzcode = data.yzcode;
            await tc_RegionRepository.UpdateAsync(model);
        }

        /// <summary>
        /// 查询列表
        /// </summary>
        /// <param name="page"></param>
        /// <param name="limit"></param>
        /// <param name="name"></param>
        /// <param name="level"></param>
        /// <returns></returns>
        public async Task<AdminResult> GetList(int page, int limit, string name, int level)
        {
            AdminResult result = new AdminResult();
            var query = from d in tc_RegionRepository.Table
                        orderby d.id //descending
                        select new tc_region()
                        {
                            id = d.id,
                             yzcode = d.yzcode, sname = d.sname, pinyin = d.pinyin, pid = d.pid, name = d.name, citycode= d.citycode, Lat =d.Lat, level =d.level, Lng =d.Lng, mername= d.mername
                        };
            if (level != 0)
            {
                query = query.Where(x => x.level == level);
            }
            if (!string.IsNullOrWhiteSpace(name))
            {
                query = query.Where(x => x.name.Contains(name));
            }
            result.count = await query.CountAsync();
            result.data = await query.Skip((page - 1) * limit).Take(limit).ToListAsync();
            result.code = 0;
            return result;
        }
    }
}
