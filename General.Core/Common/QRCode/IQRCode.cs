﻿using System.Drawing;

namespace General.Core.Common.QRCode
{
    public interface IQRCode
    {
        Bitmap GetQRCode(string url, int pixel);
    }
}
