﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Core.Common.Thumbnail
{
    public static class ThumbnailHelper
    {
        /// <summary>
        /// 获取缩略图
        /// </summary>
        /// <returns></returns>
        public static string GetThumbnailPath(string path,int width=100)
        {
            if (path.StartsWith("/upload/"))
            {
                return "/Images/GetPath?name=" + path + "&width=" + width;
            }
            return path;
        }

    }
}
