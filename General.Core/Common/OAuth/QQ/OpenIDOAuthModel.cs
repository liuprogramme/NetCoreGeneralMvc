﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Core.Common.OAuth.QQ
{
    public class OpenIDOAuthModel
    {
        public int client_id { get; set; }
        public int openid { get; set; }
    }
}
