﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Core.Common.OAuth.WeChat
{
    public static class WeChatDefaults
    {
        public const string AuthenticationScheme = "WeChat";

        public static bool OpenWechatLogin = false;

        /// <summary>
        ///  开放平台申请审核后发放的AppId
        /// </summary>
        public static string AppId = "企业申请的AppId ";

        /// <summary>
        /// 开放平台申请审核后发放的密钥
        /// </summary>
        public static string AppSecret = "企业申请的AppSecret ";

        /// <summary>
        /// 开放平台申请的回调域名
        /// </summary>
        public static string RedirectUri = "xxx.com";

        /// <summary>
        /// 认证接口
        /// </summary>
        public static readonly string AuthorizationEndpoint = "https://open.weixin.qq.com/connect/oauth2/authorize";

        /// <summary>
        /// 认证接口二维码
        /// https://open.weixin.qq.com/connect/qrconnect?appid=wxf1f84ee1c835dc1e&redirect_uri=http%3a%2f%2fwe-focus.cn&response_type=code&scope=snsapi_login&state=state#wechat_redirect
        /// </summary>
        public static readonly string AuthorizationQrEndpoint = "https://open.weixin.qq.com/connect/qrconnect";

        /// <summary>
        /// 通过code获取access_token 接口
        /// https://api.weixin.qq.com/sns/oauth2/access_token?appid=wxf1f84ee1c835dc1e&secret=a0b7f9516303199ea3fe3b39f7a930b0&code=CODE&grant_type=authorization_code
        /// </summary>
        public static readonly string AccessTokenEndpoint = "https://api.weixin.qq.com/sns/oauth2/access_token";

        /// <summary>
        /// 刷新token 接口
        /// https://api.weixin.qq.com/sns/oauth2/refresh_token?appid=wxf1f84ee1c835dc1e&grant_type=refresh_token&refresh_token=REFRESH_TOKEN
        /// </summary>
        public static readonly string RefreshTokenEndpoint = "https://api.weixin.qq.com/sns/oauth2/refresh_token";

        /// <summary>
        /// 用户信息接口
        /// https://api.weixin.qq.com/sns/userinfo?access_token=access_token&openid=open_id;
        /// </summary>
        public static readonly string UserInformationEndpoint = "https://api.weixin.qq.com/sns/userinfo";
    }

}
