﻿namespace General.Core.Common.Map
{
    public class PositionModel
    {
        public double MinLat { get; internal set; }
        public double MaxLat { get; internal set; }
        public double MinLng { get; internal set; }
        public double MaxLng { get; internal set; }
    }
}