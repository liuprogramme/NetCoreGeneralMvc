﻿using System;

namespace General.Core.Common
{
    public static class ConvertTimeHelper
    {
        /// <summary>
        /// UnixTime转时间
        /// </summary>
        /// <param name="timestamp"></param>
        /// <returns></returns>
        public static DateTime ConvertToDateTime(long timestamp)
        {
            DateTime date = DateTimeOffset.FromUnixTimeSeconds(timestamp).LocalDateTime;
            return date;
        }


        /// <summary>
        /// 时间转UnixTime
        /// </summary>
        /// <param name="date"></param>
        /// <returns></returns>
        public static long ConvertToUnixTimeSeconds(DateTime date)
        {
            DateTimeOffset dtOffset = date;
            return dtOffset.ToUnixTimeSeconds();
        }
    }
}
