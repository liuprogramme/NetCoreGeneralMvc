﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading.Tasks;

namespace General.Core.Data
{
    /// <summary>
    /// 
    /// </summary>
    /// <typeparam name="TEntity"></typeparam>
    public interface IRepository<TEntity> where TEntity : class
    {
        DbContext DbContext { get; }
        DbSet<TEntity> Entities { get; }

        IQueryable<TEntity> Table { get; }

        /// <summary>
        /// 通过主键id获取数据
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        TEntity getById(object id);
        ValueTask<TEntity> getByIdAsync(object id);
        int insert(TEntity entity, bool IsCommit = true);
        int delete(TEntity entity, bool IsCommit = true);
        Task<TEntity> GetAsync(Expression<Func<TEntity, bool>> query = null, bool ignoreQueryFilters = false);
        Task<int> CountAsync(Expression<Func<TEntity, bool>> query = null, bool ignoreQueryFilters = false);
        Task<bool> IsExistAsync(Expression<Func<TEntity, bool>> query = null, bool ignoreQueryFilters = false);
        IQueryable<TEntity> GetAllAsync(Expression<Func<TEntity, bool>> query = null, bool ignoreQueryFilters = false);
        Task<int> insertAsync(TEntity entity, bool IsCommit = true);
        Task<int> AddListAsync(List<TEntity> lists, bool IsCommit = true);
        int update(TEntity entity);
        Task<int> UpdateAsync(TEntity entity, bool IsCommit = true);
        Task<int> UpdateListAsync(List<TEntity> lists, bool IsCommit = true);
        Task<int> DeleteAsync(TEntity entity, bool IsCommit = true);
        Task<int> DeleteListAsync(List<TEntity> lists, bool IsCommit = true);

        /// <summary>
        /// 修改部分字段
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="updatedProperties"></param>
        /// <param name="IsCommit"></param>
        /// <returns></returns>
        Task<int> UpdateByFieldsAsync(TEntity entity, Expression<Func<TEntity, object>>[] updatedProperties, bool IsCommit = true);


        Task<int> UpdateListByFieldsAsync(List<TEntity> lists, Expression<Func<TEntity, object>>[] updatedProperties, bool IsCommit = true);

        /// <summary>
        /// 删除部分字段
        /// </summary>
        /// <param name="entity"></param>
        /// <param name="updatedProperties"></param>
        /// <param name="IsCommit"></param>
        /// <returns></returns>
        Task<int> DeleteByFieldsAsync(TEntity entity, bool IsCommit = true);

        Task<int> DeleteListByFieldsAsync(List<TEntity> lists, bool IsCommit = true);


        /// <summary>
        /// 执行sql命令
        /// </summary>
        /// <returns></returns>
        Task<int> ExecuteSqlAsync(string sql);
    }
}
