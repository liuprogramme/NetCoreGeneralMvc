﻿using System.Threading.Tasks;

namespace General.Core.UnitOfWork
{

    /// <summary>
    /// 工作单元接口
    /// </summary>
    public interface IUnitOfWork
    {
        int Commit();
        Task<int> CommitAsync();
    }
}
