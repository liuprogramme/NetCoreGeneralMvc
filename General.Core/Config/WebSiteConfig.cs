﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Core.Config
{
    /// <summary>
    /// 网站参数配置
    /// </summary>
    public static class WebSiteConfig
    {
        /// <summary>
        /// 是否开启网站
        /// </summary>
        public static bool WebSite_IsOpen { get; set; } = false;



        /// <summary>
        /// 网站模板配置标识
        /// </summary>
        public readonly static string WEBSITE_TEMPLATENAME_STRING = GobalConfig.SYSTEM_CONFIG_STRING+ "TemplateName";

        /// <summary>
        /// 网站模板路径
        /// </summary>
        public readonly static string WEBSITE_TEMPLATENAME_PATH = "/templete/";



        /// <summary>
        /// 导航菜单内存键名
        /// </summary>
        public readonly static string SYSTEM_NAVIGATIONMEMORYKEY_STRING = "MemoryKey_WebSite_Navigation";

        /// <summary>
        /// 图片轮播内存键名
        /// </summary>
        public readonly static string SYSTEM_SLIDERMEMORYKEY_STRING = "MemoryKey_WebSite_Slider";

        /// <summary>
        /// 友情链接内存键名
        /// </summary>
        public readonly static string SYSTEM_LINKMEMORYKEY_STRING = "MemoryKey_WebSite_Link";

        /// <summary>
        /// 文章页 最新文章内存键名
        /// </summary>
        public readonly static string WebSite_Detail_NewArticle_Key = "WebSite_Detail_NewArticle_Key";

        /// <summary>
        /// 主页 最新文章内存键名
        /// </summary>
        public readonly static string WebSite_Main_NewArticle_Key = "WebSite_Main_NewArticle_Key";

        /// <summary>
        /// 文章列表页 文章缓存键名
        /// </summary>
        public readonly static string Redis_List_Article_Key = "_Redis_List_Article_Key_";

        /// <summary>
        /// 文章列表页 所有类别内存键名
        /// </summary>
        public readonly static string WebSite_List_AllCate_Key = "WebSite_List_AllCate_Key";

        /// <summary>
        /// 自定义页面内存键名前缀
        /// </summary>
        public readonly static string SYSTEM_DIYPAGEMEMORYKEY_STRING = "MemoryKey_DiyPage_";
        
        /// <summary>
        /// 文章缓存键名
        /// </summary>
        public readonly static string Redis_Article_Key = "_Redis_Article_Key_";

        /// <summary>
        /// 文章列表信息缓存键名
        /// </summary>
        public readonly static string Redis_ArticleListInfo_Key = "_Redis_ArticleListInfo_Key_";

    }


}
