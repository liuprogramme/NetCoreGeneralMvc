﻿using System;
using System.Collections.Generic;
using System.Text;

namespace General.Core.Config
{
    public class JwtSettings
    {
        /// <summary>
        /// Token是谁颁发的
        /// </summary>
        public static string Issuer = "http://localhost:555";

        /// <summary>
        /// Token给那些客户端去使用
        /// </summary>
        public static string Audience = "http://localhost:555";

        /// <summary>
        /// 用于加密的key 必须是16个字符以上，要大于128个字节
        /// </summary>
        public static string SecretKey = "wef1561fwffwef5q1wd51z117wqd05f8ewgt515qwd";
    }
}
