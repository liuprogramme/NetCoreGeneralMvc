﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Core.Config
{
    /// <summary>
    /// 邮箱配置
    /// </summary>
    public static class MailConfig
    {
        public static string Mail_SendMail { get; set; }
        public static string Mail_Address { get; set; }
        public static string Mail_Port { get; set; }
        public static string Mail_UserName { get; set; }
        public static string Mail_PassWord { get; set; }
    }
}
