﻿using System;
using System.Collections.Generic;
using System.Text;

namespace General.Core.Config
{
    /// <summary>
    /// 全局参数配置
    /// </summary>
    public static class GobalConfig
    {
        /// <summary>
        /// 网站系统配置字符串前缀
        /// </summary>
        public readonly static string SYSTEM_CONFIG_STRING = "WebSite_";
        /// <summary>
        /// 网站邮箱配置字符串前缀
        /// </summary>
        public readonly static string SYSTEM_MAIL_STRING = "Mail_";

        /// <summary>
        /// 网站SEO配置字符串前缀
        /// </summary>
        public readonly static string SYSTEM_SEO_STRING = "SEO_";
        /// <summary>
        /// 网站系统配置_唯一标识
        /// </summary>
        public readonly static string SYSTEM_CONFIG_UNIQUEIDENTIFICATION_STRING = "WebSite_UniqueIdentification";

        /// <summary>
        /// 角色列表内存键名
        /// </summary>
        public readonly static string SYSTEM_ROLELISTMEMORYKEY_STRING = "System_RoleList";

        /// <summary>
        /// 功能列表内存键名
        /// </summary>
        public readonly static string SYSTEM_MENULISTMEMORYKEY_STRING = "System_MenuList";


        /// <summary>
        /// 微信扫码登录防止频繁刷新 键名
        /// </summary>
        public readonly static string SYSTEM_WECHATQRREDISKEY_STRING = "WeChatQR_";

        /// <summary>
        /// QQ登录防止频繁刷新 键名
        /// </summary>
        public readonly static string SYSTEM_QQAUTHREDISKEY_STRING = "QQAuth_";

        /// <summary>
        /// 微信扫码登录键名
        /// </summary>
        public readonly static string SYSTEM_WECHATUSERREDISKEY_STRING = "WeChatUser_";

        /// <summary>
        /// QQ授权登录键名
        /// </summary>
        public readonly static string SYSTEM_QQUSERREDISKEY_STRING = "QQUser_";

        #region AES加解密配置
        /// <summary>
        /// AES加解密Key
        /// </summary>
        public static string AESKEY = "aaaabbbbccccddddeeeeffffgggghhhh";
        /// <summary>
        /// AES加解密Key
        /// </summary>
        public static string AESIV = "1234567812345678";
        #endregion



    }
}
