﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace General.Core.Config
{
    public static class WebSiteTemplateConfig
    {
        /// <summary>
        /// 是否开启模板缓存
        /// </summary>
        public static bool WebSite_OpenTemplateCache { get; set; } = true;

        /// <summary>
        /// 是否开启数据缓存
        /// </summary>
        public static bool WebSite_OpenDataCache { get; set; } = true;

        /// <summary>
        /// 是否开启首页缓存
        /// </summary>
        public static bool WebSite_OpenMainCache { get; set; } = true;


        /// <summary>
        /// 页面信息列表
        /// </summary>
        public static List<TemplatePage> GetTemplatePages()
        {
            List<TemplatePage> list = new List<TemplatePage>();
            list.Add(Site_Template_Index_Info);
            list.Add(Site_Template_Header_Info);
            list.Add(Site_Template_Footer_Info);
            list.Add(Site_Template_Main_Info);
            list.Add(Site_Template_List_Info);
            list.Add(Site_Template_Detail_Info); 
            list.Add(Site_Template_Search_Info);
            return list;
        }

        /// <summary>
        /// 模板母版页
        /// </summary>
        public readonly static TemplatePage Site_Template_Index_Info = new TemplatePage() { name="index.html", remark="网站母版页" };

        /// <summary>
        /// 模板顶部页
        /// </summary>
        public readonly static TemplatePage Site_Template_Header_Info  = new TemplatePage() { name = "header.html", remark = "模板顶部页" };

        /// <summary>
        /// 模板底部页
        /// </summary>
        public readonly static TemplatePage Site_Template_Footer_Info = new TemplatePage() { name = "footer.html", remark = "模板底部页" };


        /// <summary>
        /// 模板首页
        /// </summary>
        public readonly static TemplatePage Site_Template_Main_Info = new TemplatePage() { name = "main.html", remark = "模板首页" };

        /// <summary>
        /// 文章列表页
        /// </summary>
        public readonly static TemplatePage Site_Template_List_Info = new TemplatePage() { name = "list.html", remark = "文章列表页" };

        /// <summary>
        /// 文章详情页
        /// </summary>
        public readonly static TemplatePage Site_Template_Detail_Info = new TemplatePage() { name = "detail.html", remark = "文章详情页" };

        /// <summary>
        /// 搜索页
        /// </summary>
        public readonly static TemplatePage Site_Template_Search_Info = new TemplatePage() { name = "search.html", remark = "搜索页" };
    }


    public class TemplatePage
    {
        //名称
        public string name { get; set; }
        //备注
        public string remark { get; set; }

        //内容
        public string htmlContent { get; set; }
    }

}
